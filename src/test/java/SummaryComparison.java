import hcmiu.thesis.repositories.impl.SummarizationDaoImpl;
import hcmiu.thesis.services.EvaluationService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class SummaryComparison {
    public static void main(String[] args) throws IOException {
        SummarizationDaoImpl dao = new SummarizationDaoImpl();
        try (Reader reader = Files.newBufferedReader(Path.of("summary.csv"), StandardCharsets.ISO_8859_1)) {
            CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withIgnoreHeaderCase().withIgnoreEmptyLines().withTrim());
            List<CSVRecord> records = parser.getRecords();
            records.forEach(each -> {
                try {
                    if (EvaluationService.calculateFValue(each.get(1), each.get(3)) > 0) {
                        System.out.println(EvaluationService.calculateFValue(each.get(1), each.get(3)));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
