import hcmiu.thesis.dtos.TestData;
import hcmiu.thesis.repositories.impl.NGram;
import hcmiu.thesis.repositories.impl.SummarizationDaoImpl;
import hcmiu.thesis.services.EvaluationService;
import hcmiu.thesis.utils.WordUtil;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;

public class DataFilter {
    public static void main(String[] args) throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(Paths.get("filtered-data.csv"), StandardOpenOption.CREATE);
        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
        SummarizationDaoImpl dao = new SummarizationDaoImpl();
        List<TestData> listFilterdData = EvaluationService.readCsvAsList("test-data.csv").stream().filter(each -> {
            try {
                return WordUtil.countSentences(each.getTextInput()) > 40;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }).collect(Collectors.toList());

        listFilterdData.forEach(each -> {
            try {
                csvPrinter.printRecord(each.getTextInput());
                System.out.println(listFilterdData.indexOf(each));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
