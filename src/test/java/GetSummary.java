import hcmiu.thesis.repositories.impl.SummarizationDaoImpl;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class GetSummary {
    public static void main(String[] args) {
        SummarizationDaoImpl dao = new SummarizationDaoImpl();
        try (
                Reader reader = Files.newBufferedReader(Path.of("filtered-data.csv"), StandardCharsets.ISO_8859_1);
                CSVPrinter printer = new CSVPrinter(Files.newBufferedWriter(Path.of("summary-10.csv"), StandardOpenOption.CREATE), CSVFormat.DEFAULT);
        ) {
            CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT);
            List<CSVRecord> records = parser.getRecords();
            records.forEach(each -> {
                try {
                    printer.printRecord(records.indexOf(each), dao.getSummary(each.get(0), 10), dao.getLSA(each.get(0), 10));
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                System.out.println(records.indexOf(each) + " - " + dao.getLSA(each.get(0), 5));
            });
            printer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
