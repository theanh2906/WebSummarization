INSERT INTO TBL_CATEGORY VALUES (null,'Animals','#df859e');
INSERT INTO TBL_CATEGORY VALUES (null,'Architecture','#ac97b4');
INSERT INTO TBL_CATEGORY VALUES (null,'Arts','#66abdd');
INSERT INTO TBL_CATEGORY VALUES (null,'Banking','#682a43');
INSERT INTO TBL_CATEGORY VALUES (null,'Business','#d8122f');
INSERT INTO TBL_CATEGORY VALUES (null,'Community','#e33617');
INSERT INTO TBL_CATEGORY VALUES (null,'Economics','#52e532');
INSERT INTO TBL_CATEGORY VALUES (null,'Education','#fff63a');
INSERT INTO TBL_CATEGORY VALUES (null,'Entertainment','#f05dc5');
INSERT INTO TBL_CATEGORY VALUES (null,'Internet','#c91192');
INSERT INTO TBL_CATEGORY VALUES (null,'Lifestyle','#00580a');
INSERT INTO TBL_CATEGORY VALUES (null,'Medical','#ff6666');
INSERT INTO TBL_CATEGORY VALUES (null,'School','#ffa500');
INSERT INTO TBL_CATEGORY VALUES (null,'Science','#00deff');
INSERT INTO TBL_CATEGORY VALUES (null,'Sports','#9875ef');
INSERT INTO TBL_CATEGORY VALUES (null,'Technology','#292040');
INSERT INTO TBL_CATEGORY VALUES (null,'Travel','#10adba');
INSERT INTO TBL_CATEGORY VALUES (null,'Weather','#e9967a');
INSERT INTO TBL_CATEGORY VALUES (null,'Comedy','#e3f252');

INSERT INTO TBL_STATUS VALUES (null,1,'Active');
INSERT INTO TBL_STATUS VALUES (null,2,'Inactive');
INSERT INTO TBL_STATUS VALUES (null,3,'Outdated');
INSERT INTO TBL_STATUS VALUES (null,4,'Draft');

INSERT INTO TBL_ACCOUNT VALUES (null,'admin','$2a$12$prsh2EJQLkTwRGKAhQhWF.LAXbMOtux7TAzEXZpHZ7x3b6oTaUp2.',1,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'theanh2906','$2y$10$Sz0n7z.WmzdoRKMyrQmezOFK.OUOSd75axIzl6sX9Oxc.KWvK6D0C',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user1','$2y$10$owAE4vBjO.boZPEg30Myuu8MEuu4IbVjfHIZEh4VkB.LuLpbIgs3u',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user2','$2y$12$yg9I/inVsKGQE9PBHLwHKu/67PCb/hwd4KVr2bhAQWqtWTGuE85Um',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user3','$2y$12$kH4iLOrrGmwz690a1PWmYug8IzvBhgdgfFDYmkCc/9yiYXoBYtkQe',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user4','$2y$12$cHmMUItEe.sdtftiacnMD.9tXKYOkU3dI1/s/Y9I1ppC1vvLlx0xO',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user5','$2y$12$Q3TsE7bRFSpIix3s280Bk.0p3pGr/qq7lvYuG537F6H7UFydkV50e',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user6','$2y$12$L3faX4UU5yLfa4fL4xUiRuf.mt3er/6HxW2bvLI6bYNEbdQNuub2e',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user7','$2y$12$6wkkrd2fd3ZpHawBED6ZR.AvYYomgMKmc08xiec96aYB5qVjG64Ra',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user8','$2y$12$dJMoeuIcJJin.G6gmLtuuuJb/2A3Q4Jzs5NlJCk6Og/UFdyodWUIy',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user9','$2y$10$IWuuVRCdBo/8Bl9371ghH.QTYnsJ36HEm3621E3s8.vma5CjcTozi',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user10','$2y$12$Rft5EjxguWkQPu70JGO.EOwQduGcm4A7AKxqYafCr6P/UwXJFHRFu',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user11','$2y$12$y53leJTIQFeRjJX4jntdF.0/1tXvvn9eomkab3USWkB8Z1jcE5rz2',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user12','$2y$12$QQ3avhNO3ak0cMyWl7rAUu6IMhrcqjqDRHXapK3vX.HHp6bNVN.6y',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user13','$2y$12$IPrMFHGtFlf46eSI24yWn.LiCZ99QjzN0mYOYoBnjerYJ5RaBpJ1C',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user14','$2y$12$LFuz4dnGDnmLxonyaUvoseNn2oKdKLc5COUuiWQ8lYtM.SSGcMWeS',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user15','$2y$12$FE7Fgu0athhU.l0TdSJQpO0rE/dbyV/euGc6iMUauN4pElYI2g32e',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user16','$2y$12$8Pe2VwQMoWM15RbX6HWmuu3obrGX4geNU0SR.Lwj/vldWsPWN7J2m',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user17','$2y$12$1X5MELtnLldYOhNa.c4EWunrFdcexCqgSG9JXyrHN5a7NuviuxKXm',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user18','$2y$12$gCMPWIfU9xiMRRGqFSt9b.mwfHSSkWVAOU9g10rpKEwlHG9rWpNO',2,true,'2021-12-16');
INSERT INTO TBL_ACCOUNT VALUES (null,'user19','$2y$12$/h5ONDDI133Y6Jxqjycbze5Pa2gGRAScab3tJYS5X67BKECKdp7Aa',2,true,'2021-12-16');

INSERT INTO TBL_USER VALUES (null,1,'Administrator','1996-06-29','admin@gmail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,2,'Tang The Anh','1996-06-29',null,null,null,null,null);
INSERT INTO TBL_USER VALUES (null,4,'SMITH','1996-06-29','smith@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,5,'JOHNSON','1996-06-29','johnson@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,6,'WILLIAMS','1996-06-29','williams@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,7,'JONES','1996-06-29','jones@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,8,'BROWN','1996-06-29','brown@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,9,'DAVIS','1996-06-29','davis@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,10,'MILLER','1996-06-29','miller@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,11,'WILSON','1996-06-29','wilson@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,12,'MOORE','1996-06-29','moore@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,13,'TAYLOR','1996-06-29','taylor@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,14,'ANDERSON','1996-06-29','anderson@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,15,'THOMAS','1996-06-29','thomas@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,16,'JACKSON','1996-06-29','jackson@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,17,'WHITE','1996-06-29','white@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,18,'HARRIS','1996-06-29','harris@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,19,'MARTIN','1996-06-29','martin@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,20,'THOMPSON','1996-06-29','thompson@mail.com',null,null,null,null);
INSERT INTO TBL_USER VALUES (null,21,'LUCAS','1996-06-29','lucas@gmail.com',null,null,null,null);

INSERT INTO TBL_POST VALUES (null,2,'U.S. Launches Auto Import Probe, China Vows To Defend Its Interests','<p>WASHINGTON (Reuters) - The Donald Trump administration has launched a national security investigation into car and truck imports that could lead to new U.S. tariffs similar to those imposed on imported steel and aluminum in March.</p>

	<p>The national security probe under Section 232 of the Trade Expansion Act of 1962 would investigate whether vehicle and parts imports were threatening the industry&rsquo;s health and ability to research and develop new, advanced technologies, the Commerce Department said on Wednesday.</p>

	<p>&ldquo;There is evidence suggesting that, for decades, imports from abroad have eroded our domestic auto industry,&rdquo; Commerce Secretary Wilbur Ross said in a statement, promising a &ldquo;thorough, fair and transparent investigation.&rdquo;</p>

	<p>Higher tariffs could be particularly painful for Asian automakers including Toyota Motor Corp, Nissan Motor Co, Honda Motor Co and Hyundai Motor Co, which count the United States as a key market, and the announcement sparked a broad sell-off in automakers&rsquo; shares across the region.</p>

	<p>The governments of Japan, China and South Korea said they would monitor the situation, while Beijing, which is increasingly eyeing the United States as a potential market for its cars, added that it would defend its interests.</p>

	<p>&ldquo;China opposes the abuse of national security clauses, which will seriously damage multilateral trade systems and disrupt normal international trade order,&rdquo; Gao Feng, spokesman at the Ministry of Commerce, said at a regular news briefing on Thursday which focused largely on whether Beijing and Washington are making any progress in their growing trade dispute.</p>

	<p>&ldquo;We will closely monitor the situation under the U.S. probe and fully evaluate the possible impact and resolutely defend our own legitimate interests.&rdquo;</p>

	<p>Courting Voters The probe comes as Trump courts voters in the U.S. industrial heartland ahead of mid-term elections later this year, and opens a new front in his &ldquo;America First&rdquo; trade agenda aimed at clawing back manufacturing jobs lost to overseas competitors.</p>

	<p>It could raise the costs for overseas automakers to export vehicles and parts to the world&rsquo;s second-largest auto market.</p>

	<p>Growing trade tensions over cars and car parts, particularly with China, could raise risks for U.S. companies expanding their presence in the country, signs of which are already emerging.</p>

	<p>Earlier this month, Reuters reported that Ford Motor Co&rsquo;s imported vehicles were being held up at Chinese ports, adding to a growing list of U.S. products facing issues at China&rsquo;s borders.</p>

	<p>The majority of vehicles sold in the United States by Japanese and South Korean automakers are produced there, but most firms also export to the U.S. from plants in Asia, Mexico, Canada and other countries.</p>

	<p>Roughly one-third of all U.S. vehicle imports last year were from Asia.</p>','24-05-18',null,1,null,'David Shepardson, Jeff Mason',null);
INSERT INTO TBL_POST VALUES (null,2,'An Alert, Well-Hydrated Artist in No Acute Distress—Episode Thirty-Five: The Birth of a Book, the End of a Story','<p>After the thrill of seeing Hadley&rsquo;s murals on the walls of the Montana State Capital building, coming home to my novel that was growing roots in a file on my laptop was hard. The disappointment I felt in myself was directly proportional to the admiration I felt for Hadley. It was impossible not to compare our trajectories. Hadley, plagued as she&rsquo;d been with serious health issues all her life, had turned out many notable projects in her 38 years, culminating in a historic, grand-scale commission. Here I was, on the verge of 60, and I hadn&rsquo;t managed to get Dream House, the project that most mattered to me, out into the world. I&rsquo;d witnessed Hadley&rsquo;s deep satisfaction, unveiling the strongest and most celebrated work of her life. I wanted that!</p>

	<p>The week following my trip to Montana, I attended a rousing orchestral performance. I found myself imagining a violinist playing her instrument in her room for years on end with no clear prospect of ever being heard. An absurd notion, as music is meant to be listened to. We don&rsquo;t think of writing as a performing art. But like musicians, writers work assiduoaulsy to craft an affecting expression of their ideas and stories in order to touch an audience with their words. Writers, I glumly mused as the orchestra played on, can write for years without having their work read.</p>

	<p>Away doom and gloom! I couldn&rsquo;t let my discouragement quash my determination. I began making inquiries about self-publishing. I had no well-considered bias against this route to publication; I just didn&rsquo;t feel I had the stamina it takes to make it happen. No sooner had I begun my research, than an email popped up that made my heart gallop. Jay, my Dream House editor, had a colleague who was interested in publishing the book.</p>

	<p>It was tempting to get carried away by the excitement of this news. But I didn&rsquo;t share it even with my husband, Lewis, because the only thing worse than riding the rollercoaster of publication hopefulness that had begun four years earlier was taking those closest to me along for the ride. Two weeks after Hadley&rsquo;s mural unveiling, in a state of suspended belief, I drove downtown to meet with Lisa McGuinness, publisher at Yellow Pear Press. We hit it off and right then and there, nailed down details and deadlines to meet for publishing in the fall, November 2015.</p>

	<p>Within eight weeks of my meeting with Lisa, my novel had a cover and I was proofreading the galleys. Every week, I let the new reality sink in a little more, but I still felt unable to celebrate. In part, this was because publishing had been a dream for so long that it felt too good to be true. Also, with many hands involved in its production, I fretted about whether the physical book would have the qualities I hoped for. And then there was the obvious: apprehension about how the novel would be received.</p>

	<p>Another reason I felt excitement bubbling up only at discreet moments was not related to anxieties about my novel, but rather, what was happening in my day-to-day, i.e., life, which goes on despite the alternate realities our work sometimes constructs for us. Sylvia, after a short remission, was not winning her fight with cancer. The 07y ablation she&rsquo;d had on the small lesion on her liver appeared to trigger an inflammatory response, resulting in a new proliferation of cancer cells. By February, she was housebound, except when she needed to see her d10or, and was spending much of her days sleeping. One Saturday evening in mid-March, the water heater on the second floor of her house began leaking through the floor. The plumber who came on Monday used a blowtorch too close to a fire sprinkler in the water heater closet, setting off a monsoon in the house. Sylvia&rsquo;s daughter, Meredith, home at the time, called 911 and me. Then, she led her bathrobed mother down into the street and helped her and their dog into their parked car. When I arrived, there was 05hem: two fire trucks, water everywhere, my stunned, sliver of a friend sitting in the car out front. I drove her to my house and set her up in bed.</p>

	<p>Sylvia, her husband Peter, Meredith, and their Golden Retriever stayed with us for a week while their house dried out. It was profoundly affecting to witness the tender, complicated care Peter and Meredith gave Sylvia, and to have one of my dearest friends under my roof at the end of her life. When she was sleeping, I tried to carry on with the daily tasks that were moving my book forward, but my awareness of Sylvia&rsquo;s ephemerality kept me in the present moment. I didn&rsquo;t want to miss something important. I savored the times she asked me to help shift her tiny frame in the bed, each brief conversation we shared. &ldquo;I don&rsquo;t want to keep going like this,&rdquo; she told me one day. We talked about what&rsquo;s within a patient&rsquo;s power when they are ready to stop suffering and say goodbye. What could be more important, and yet how many times in life do we get the chance to talk this way with someone we love? One morning when we were chatting, she looked deep in thought for a few moments and I braced myself for something difficult. Finally, she said, &ldquo;I probably won&rsquo;t be able to eat it, but just in case, can you save me some of the Bolognese sauce you made for dinner?&rdquo; She laughed. She was such a foodie. She&rsquo;d been eating Gerber baby arrowroot biscuits, mashed potatoes and ground chicken for days; no wonder she craved some zest!</p>

	<p>That week, I barely thought about Parkinson&rsquo;s, which felt like a mere annoyance. But I thought a lot about what awaits me and everyone close to me at the end of our lives. During Sylvia&rsquo;s stay, I talked to Hadley and she recalled her last days with her stepfather, Charlie. Our conversation about dying was disorienting, as if I were looking through a camera lens at the four of us&#8202;&mdash;&#8202;Hadley, Sylvia, Charlie, and myself&#8202;&mdash;&#8202;trying to adjust the focal length to correspond with our (unspoken) perceptions of where we were on the timelines of our lives.</p>

	<p>In late March, I sat on Sylvia&rsquo;s bed in the bright California sunshine streaming through the windows. She was waifish, every bone explicit. Always meticulously groomed, she worried aloud about how long her fingernails had grown and I asked her if she&rsquo;d like me to cut them. Sylvia was a private person, and I felt honored when she said yes. As I held each of her cool, delicate fingers in my hand and clipped, I took my time, relishing the intimacy of this simple task, taking it in as Sylvia&rsquo;s last gift to me. She died several days later with Peter and Meredith by her side.</p>

	<p>Winter and spring were a blur of proofreading the Dream House galleys&#8202;&mdash;&#8202;every word and punctuation mark, six times (by the time the book went to print, I probably could&rsquo;ve told you to pick a word, any word, and I would&rsquo;ve been able to turn to the page where it was written)&mdash;and pulling together promotional material at the behest of the publicist. Among other things, this included setting up an author Facebook page and a website, which I was lucky enough to have our son, Tobias, design. I learned how to describe the book in as few, hopefully enticing, phrases as possible: &ldquo;An architect of houses searching for home,&rdquo; &ldquo;...how we shape and are shaped by our houses,&rdquo; &ldquo;What makes a house feel like home?&rdquo; As tedious as the production period was at times, after years of editing Dream House with nothing but hope and encouragement to keep me motivated, I gobbled up the tasks before me, knowing that there would be a monumental payoff. Finally, I was able to put to bed all my sad feelings that are best described by the old question, &ldquo;If a tree falls in a forest and no one is there to hear it, does it make a sound?&rdquo;</p>

	<p>In 07y, Lisa met me for lunch to present my printed book. When she passed it across the table, I was unprepared for the emotional rush that sent me diving for my napkin to brush away tears. To me, the book was an exquisite object. The quiet but evocative jacket featuring an image of the Maine house where I was raised perfectly captures the tone and landscape of the novel; the size and weight of the book is modest but inviting. I hadn&rsquo;t dared to hope the physical embodiment of my story and the story itself, in which I explored everything that most moved me: space and architecture, love and motherhood, pain and healing, ocean, sky and trees, would be so well integrated, so...right.</p>

	<p>A week before the book&rsquo;s publishing date, in late 10ober 2015, we threw a party. I dubbed it the &ldquo;If not now, when?&rdquo; party. I&rsquo;d been casting about for an excuse to have a very big party for years and a book event seemed more fun than waiting for a milestone birthday, which, especially when you have a progressive, degenerative disease, is not necessarily a cause for celebration. The party was a blast. Local friends and my neurologist came. Friends from the other coast as well as our daughter Elena, my sisters, their husbands and some of their offspring flew in. Tobias&rsquo;s band played and I danced with the freewheeling women in my writing group. Lisa set up a bookselling table and friends generously stood in line to purchase books and have me sign them. While turning my Sharpie loose on book after book, it finally became real to me: my book!</p>

	<p>Did I have Parkinson&rsquo;s? Not that night! I spoke into a microphone for the first time in my life. I was nervous, since presenting to groups has never been easy for me, but being in that crowd of well-wishers was like soaking in a warm bath.</p>

	<p>2017-01-27-1485560556-3601852-BoooksIncshelfsmall.JPG</p>

	<p>From November through April, I talked about Dream House in the west and in New England at bookstores, private parties, newspaper and radio interviews, seven book groups and an architecture school. Awesome family and friends from around the country facilitated opportunities for me to speak and sell books. Mindful of my energy&rsquo;s ebb and flow, I only scheduled events where I knew people who would fill the chairs.</p>

	<p>2017-01-27-1485560690-3016720-ReadingSmall.JPG</p>

	<p>I never stopped feeling nervous when approaching the podium, afraid I&rsquo;d go blank in the middle of a thought. I did a couple of times, but managed to recover well enough. After describing the inspirations for the novel, I would read a couple of passages and feel my anxiety drop away. Speaking the words I&rsquo;d written, I re-experienced the passion I&rsquo;d felt when I started the book years ago. It was powerful to feel I was engaging my listeners&rsquo; emotions and imagination, not by selling plot or drama but simply through my choice and arrangement of words.</p>

	<p>Finally, I was out of my room, playing my music. People were listening.</p>

	<p>2017-01-27-1485561193-3945649-RiceLibrarySmall.jpg</p>

	<p>I didn&rsquo;t push much beyond my limitations to publicize Dream House, so the post-publication experience was entirely positive. Even at the event in my hometown, 3000 miles away, where I feared God-knows-who would surface to scold me with a wagging finger for airing my family&rsquo;s troubles, people were warm, inquisitive and even a little proud of their homie author. In addition to old family friends I hadn&rsquo;t seen in decades, two of my first childhood girlfriends were in that room, as well as my first love; their presence added a distracting tenderness to the occasion. Everywhere I went, I filled with gratitude for people&rsquo;s inquisitiveness, enthusiasm, and their persistent pursuit of literature.</p>

	<p>That Lewis was the best roadie ever made all the difference. It was as if he&rsquo;d read a manual on how to support your Parkie wife while she&rsquo;s on a book tour. He hefted my carryon into the overhead bin. Took my hand in the airport when he saw my leg dragging. Schlepped boxes of books and ran peoples&rsquo; credit cards at private events. While at our Airbnb, he dashed to the market for groceries, made me coffee every morning and, in the evenings before my book talks, a mini margarita to boost my confidence. (Trust me; it works.) Each day, he rode a bike for an hour and a half but insisted he wasn&rsquo;t too tired to go back out and take a three mile walk with me, because he knows that exercise is what keeps my body from shutting down altogether. He was excited to greet the wonderful old friends who came out of the woodwork. Throughout the &ldquo;season of Dream House,&rdquo; Lewis expressed no complaints. He had lost his partner in architecture years before, but was now proud to crow about her book to anyone who&rsquo;d listen. He was thrilled for me, but also, like many partners of those with debilitating conditions, he understands the importance of turning up the light on life&rsquo;s joyful passages and significant accomplishments.</p>

	<p>So, how did it feel to finally be published at 60? Besides joy, another feeling: relief when I finished my book tour. Taking stock of my Parkinson&rsquo;s progression, I had a sense that publication came not a moment&#8202;&mdash;&#8202;or at least year&#8202;&mdash;&#8202;too soon. Also a relief was to be done with the story that had been parked in my brain like a giant RV full of claustrophobic family members, and to clear out my cabinet filled with paper drafts, some dating back to the late 20th century. I could move on.</p>

	<p>Parkinson&rsquo;s added to my life a new community of friends like Hadley with whom I can share the experience of our disease. The release of Dream House provided another avenue for making meaningful connections. Friends and total strangers have written to me about why they resonated with the novel; the territories that I&rsquo;d burned to explore ignited in them vivid memories and powerful emotions. I&rsquo;ve felt honored and delighted that they&rsquo;ve shared, sometimes in intimate detail, tales of their complicated relationships with a house, with a parent, or their children. This&#8202;&mdash;&#8202;touching the hearts and minds of my readers and in return, being moved by their stories&#8202;&mdash;&#8202;was the least expected but greatest reward of realizing my long-time dream.</p>','2021-12-16',null,1,null,'Catherine Armsden',null);
INSERT INTO TBL_POST VALUES (null,3,'Will Smith Joins Diplo And Nicky Jam For The 2018 World Cup’s Official Song','<p>Love actually turned to matrimony for Hugh Grant.</p>

	<p>The 57-year-old actor married his longtime girlfriend, Anna Eberstein, in London on Friday, according to reports.</p>

	<p>The couple was photographed celebrating with friends and family outside the Chelsea Register Office after a civil ceremony. Their intent to wed had already been posted in a public notice.</p>

	<p>Eberstein, a 39-year-old Swedish TV producer, and Grant have three young children together ranging in age from 5 to a few months old.</p>

	<p>It is the first marriage for Grant, who had a long-term romance with actress Elizabeth Hurley. The &ldquo;About A Boy&rdquo; and &ldquo;Notting Hill&rdquo; star also had a relationship with Tinglan Hong that produced a son and a daughter, ages 5 and 6.</p>

	<p>Grant&rsquo;s official vows 05 surprise some, given his public stance on marriage.</p>

	<p>&ldquo;I think there&rsquo;s something unromantic about marriage. You&rsquo;re closing yourself off,&rdquo; he said on &ldquo;The Howard Stern Show&rdquo; in 2016.</p>

	<p>However, he also conceded that &ldquo;there&rsquo;s lovely aspects of it sometimes, if you marry exactly the right person, your best friend, and it&rsquo;s cozy and it&rsquo;s lovely.&rdquo;</p>','2021-12-16',null,1,null,'Ron Dicker',null);
INSERT INTO TBL_POST VALUES (null,3,'The Controversial Way Some California Schools Are Handling Students’ Misbehavior','<p>The two 9th-grade girls heard the laughing the minute they walked into their third-period class that December morning at Oakland&rsquo;s Fremont High School. And they knew why: a video of one of the girls being slapped by a classmate had gone viral among students on social media.</p>

	<p>It was one of those moments that could have gone bad in a hurry &mdash; like so many others had at Fremont High, a school that had more suspensions last year than any other in the Oakland Unified School District.</p>

	<p>Both girls (whose names are being withheld to protect their privacy) acknowledged later that their first instinct was to lash out at their snickering classmates. But they didn&rsquo;t do that. Instead, they left the classroom and walked down the hall to Tatiana Chaterji&rsquo;s room.</p>

	<p>Chaterji is Fremont High&rsquo;s restorative justice facilitator and among a growing number of educators in Oakland Unified charged with changing the district&rsquo;s approach to behavioral issues through restorative practices. This work departs from traditional school discipline in that it focuses less on punishment and more on righting wrongs and building healthy relationships within the school.</p>

	<p>During the previous period, the two girls had participated in a community building circle, a cornerstone of restorative justice in which students gather in a circle, talk about the difficulties of their daily lives and work on responding to them in a healthier way.</p>

	<p>&ldquo;What would have happened had you stayed (in the classroom)?&rdquo; Chaterji asked the girls after they had told her their story.</p>

	<p>&ldquo;They would have said some things, then I would have said some things&#8230;then things could have gotten ugly,&rdquo; said the more assertive of the two, who was wearing an ankle monitor from the Alameda County Juvenile Probation Department.</p>

	<p>Had things gotten out of hand, punches might have been thrown. That would&rsquo;ve led to an office referral and perhaps suspensions. Such an outcome would be an unfortunate but not uncommon occurrence at Fremont, which, according to district data, suspended 151 students during the 2016-17 school year.</p>

	<p>Tatiana Chaterji, the restorative justice facilitator at Fremont High School in the Oakland Unified School District. DAVID WASHBURN / EDSOURCE Tatiana Chaterji, the restorative justice facilitator at Fremont High School in the Oakland Unified School District. Fremont High hired Chaterji last summer as part of a larger effort to improve the school&rsquo;s climate and cut down on suspensions. The school also employs three case managers who work to alleviate conflicts that crop up in classrooms before they become office referrals.</p>

	<p>&ldquo;People&rsquo;s trust in the process is growing,&rdquo; Chaterji said. &ldquo;The leadership has really shifted to prioritize [restorative justice]&#8230;we are at an exciting moment, but it&rsquo;s just the start.&rdquo;</p>

	<p>A new approach to an old problem</p>

	<p>Small victories like the one that morning at Fremont High are being won to varying degrees in schools throughout California. Over the past decade, a mountain of research has shown that the so-called zero-tolerance approach to misbehavior, characterized by stringent rules and harsh punishments, largely doesn&rsquo;t work.</p>

	<p>In particular, studies have shown unequivocally that students of color are suspended and expelled at disproportionately higher rates than their white peers, which has forced a reassessment of school discipline in many places throughout the nation.</p>

	<p>Teachers and administrators have come to realize that a student&rsquo;s range of experiences &mdash; their home life, their neighborhood and the overall atmosphere of the school &mdash; has an outsized impact on their behavior in class. Research shows that by gaining insight into these experiences and building stronger relationships with students, educators can address a number of behaviors without having to resort to suspensions and other punitive methods of discipline.</p>

	<p>This awakening, along with intense pressure on districts from the state in recent years to cut down on suspensions, have spawned a number of behavioral support programs under the umbrella of social/emotional learning, including Positive Behavioral Intervention and Supports (PBIS) and Multi-Tiered Systems of Support (MTSS).</p>

	<p>Interwoven in these approaches is the idea of restorative justice, which has both captured the imagination of many youth advocates and educators and generated controversy.</p>

	<p>In recent years, some of the state&rsquo;s largest districts have made significant investments in restorative justice:</p>

	<p>Oakland Unified budgeted roughly $2.5 million for restorative justice in the 2017-18 school year, which pays for 35 facilitators and a districtwide coordinator. The Los Angeles Unified School District budgets more than $10 million annually for restorative justice and has a goal of implementing the practices in each of its more than 900 schools by 2020. Following the lead of Los Angeles Unified, the San Diego Unified School District board last year approved a &ldquo;School Climate Bill of Rights&rdquo; that is centered on restorative practices. The board also approved a nearly $800,000 budget for restorative justice in 2017-18, which pays for a districtwide program manager along with several other staff members. The Santa Ana Unified School District received a multi-year, $3 million federal grant to implement restorative practices in schools throughout the district. Although the terms restorative justice and restorative practices were largely unheard of in the school setting as recently as a decade ago, the work in many respects builds on conflict mediation strategies that schools have used since the 1990s.</p>

	<p>Yet many see restorative justice as groundbreaking because at its core is a repudiation of the punitive model that has been the foundation of school discipline in this country since the days of the one-room schoolhouse.</p>

	<p>A community building circle in Tatiana Chaterji&amp;#39;s classroom at Fremont High School in the Oakland Unified School District TATIANA CHATERJI A community building circle in Tatiana Chaterji&rsquo;s classroom at Fremont High School in the Oakland Unified School District. Because their use in the school setting is so new, there is scant research on the long-term effectiveness of restorative practices. But officials in districts that have devoted significant resources to them say they&rsquo;ve led directly to fewer suspensions and better school climates.</p>

	<p>&ldquo;We have seen a drastic reduction in suspensions and RJ (a commonly used shorthand for the practices) is a big reason for it,&rdquo; said Deborah Brandy, Los Angeles Unified&rsquo;s director of district operations, which oversees restorative justice programs.</p>

	<p>&ldquo;We&rsquo;ve also seen a reduction in truancy rates&#8230;and it goes beyond the data. Parents feel more welcome at their school sites; students remarked (in climate surveys) that their teachers seem more caring.&rdquo;</p>

	<p>While awareness of restorative practices is high among school officials statewide, relatively few districts outside major urban centers have well-established programs, EdSource found through interviews and a survey.</p>

	<p>The most common sentiment expressed among nearly a dozen superintendents, principals and other officials interviewed was cautious optimism, with the caveat that finding resources to devote to it is a challenge.</p>

	<p>&ldquo;There is certainly an interest and heightened awareness,&rdquo; said Tamara Clay, who is director of the El Dorado County Special Education Local Plan Area. &ldquo;And system change can be easier in small rural areas like ours &mdash; but it&rsquo;s harder in that our superintendents don&rsquo;t have the capacity.&rdquo;</p>

	<p>Too much too soon?</p>

	<p>While it is difficult to find anyone &mdash; administrators, teachers, students or parents &mdash; who disagrees with the core principles of restorative justice, a fair number of critics say it&rsquo;s been oversold as a quick fix. And, in some instances, they say it&rsquo;s contributed to more chaotic school environments.</p>

	<p>Los Angeles Unified&rsquo;s efforts have drawn criticism from some teachers&rsquo; union officials who say the district has launched an aggressive implementation plan without sufficiently taking into account how the timetable is affecting students and teachers at the ground level.</p>

	<p>&ldquo;The LAUSD idea is that in three years&rsquo; time we&rsquo;ll just train all the teachers and we&rsquo;ll be done,&rdquo; said Daniel Barnhart, who is vice president of secondary schools for United Teachers of Los Angeles. &ldquo;It is a recipe for resentment and for teachers to not make a change they 05 want to make because there is no real support.&rdquo;</p>

	<p>Belia Saavedra, director of restorative justice in schools for the Long Beach-based California Conference for Equality and Justice (CCEJ), said most teachers she works with embrace restorative justice &mdash; but she has encountered pockets of resistance in both Long Beach and Los Angeles schools.</p>

	<p>&ldquo;More than a few teachers will tell you that RJ is the removal of punishment without a replacement for accountability,&rdquo; Saavedra said, referring to concerns that there aren&rsquo;t sufficient consequences. &ldquo;If RJ is coming to their school they see it as the wild, wild West.&rdquo;</p>

	<p>LA Unified&rsquo;s Brandy does not dispute the reports of pushback, but says the concerns fade once teachers and administrators see the district&rsquo;s commitment to the approach.</p>

	<p>&ldquo;Because the district has been very steadfast we are getting more and more buy-in,&rdquo; Brandy said. &ldquo;In the first year, we received a lot of pushback. In the second year, people started calling me, asking me &lsquo;When am I going to get the RJ training?&rsquo;&rdquo;</p>

	<p>The restorative justice room at Roosevelt High School in the Los Angeles Unified School District. 07E LEOPO The restorative justice room at Roosevelt High School in the Los Angeles Unified School District. Brandy&rsquo;s assertions notwithstanding, the issues being raised are real and indicative of the pendulum swinging too quickly away from traditional discipline, argues Max Eden, a senior fellow specializing in education policy for the Manhattan Institute, a conservative think tank based in New York City.</p>

	<p>Eden says his research shows that students report feeling less safe when districts issue mandates to reduce suspensions and in their place offer alternatives like restorative justice and PBIS.</p>

	<p>&ldquo;There is more immediate evidence that the reforms are creating a crisis rather than solving one,&rdquo; Eden said, pointing to studies done in New York City, Philadelphia and Virginia. &ldquo;If it were being approached as a complement to traditional discipline I would be bullish, but given that it&rsquo;s being looked at as a substitute, I&rsquo;m bearish.&rdquo;</p>

	<p>Daniel Losen, who is director of UCLA&rsquo;s Center for Civil Rights Remedies, takes issue with Eden&rsquo;s arguments on a couple of levels.</p>

	<p>First, Losen said Eden is cherry-picking indicators to make schools seem more unsafe than they actually are. Secondly, he sees in Eden a failure to acknowledge that there is strong evidence showing that suspensions and other isolating punishments are harmful to students, especially students of color.</p>

	<p>&ldquo;No one wants the reform efforts to yield something worse than before,&rdquo; Losen said. &ldquo;But we have to reject the status quo. Schools are doing things that are harmful to kids right now, and we need to stop that &mdash; their civil rights are being violated.&rdquo;</p>

	<p>A winding road to progress</p>

	<p>It is because of disagreements like the one between Eden and Losen that Sonia Llamas, Santa Ana Unified&rsquo;s assistant superintendent for school performance and culture, spends a lot of her time documenting her district&rsquo;s success with restorative practices and showing how they help its bottom line.</p>

	<p>Five years ago, Santa Ana Unified had nearly 9,800 days of suspensions, Llamas said, which cost the district about $680,000 because state funding is calculated based on average daily enrollment. Since then, thanks to a grant from the federal Department of Education, the district has invested more than $3 million in restorative justice and related programs and seen its suspensions drop by 75 percent.</p>

	<p>&ldquo;People can talk a good talk, but you need strong data to show what&rsquo;s working,&rdquo; Llamas said. &ldquo;It is really hard to cut something that is showing impact.&rdquo;</p>

	<p>That being said, Llamas and other proponents emphasize that transforming a school&rsquo;s climate and culture often happens in fits and starts and requires commitment and patience from schools and communities.</p>

	<p>&ldquo;The ability to do RJ is based on where a school and its community are at and start from there,&rdquo; said David Yusem, Oakland Unified&rsquo;s restorative justice coordinator. &ldquo;Right now, there are some schools, just like some communities, that are ready for RJ and it can come in really nicely. Then there are other schools that are fractured and it&rsquo;s tough to implement it.&rdquo;</p>

	<p>John Jones III recently moved to Oakland from Portland and his son, a 9th-grader at Fremont High, has had trouble adjusting to his new school. Jones, who works for a community group as a restorative justice facilitator, said the school&rsquo;s handling of altercations his son had with a teacher showed the progress Fremont has made as well as how far it still has to go.</p>

	<p>&ldquo;My biggest critique is that I wasn&rsquo;t notified of the situation until months afterwards,&rdquo; Jones said. &ldquo;Once there is the first inkling of a problem, parents should be brought in&#8230;the old proverb is true, it does take a village to raise a child &mdash; and it&rsquo;s important that everyone is on the same page.&rdquo;</p>

	<p>While they acknowledge their progress has not gone in a straight line, the staff at Fremont High feel they are slowly getting on the same page. The school is on track to cut suspensions in half from last year, said Co-Principal Tom Skjervheim.</p>

	<p>&ldquo;Part of the challenge is we have lots of students who need support in any given day,&rdquo; Skjervheim said. &ldquo;[But] now that we have a system where RJ can live &mdash; it is setting us up for more success.&rdquo;</p>

	<p>When asked whether she learns more from being suspended or going through restorative justice when she gets in trouble for fighting, the 9th-grade girl who had sought Chaterji&rsquo;s counsel after the problems in her third-period class rolled her eyes. &ldquo;It&rsquo;s all a waste of time,&rdquo; she said.</p>

	<p>But when pressed further, she gave a clear-headed comparison of the two approaches.</p>

	<p>&ldquo;I could be getting into a fight with someone and get suspended. Then I come back and it could still be a fight,&rdquo; she said. &ldquo;If I don&rsquo;t get suspended and we talk it out, there is a higher chance of there being no more problems.&rdquo;</p>','2021-12-16',null,1,null,'EdSource, Editorial Partner',null);
INSERT INTO TBL_POST VALUES (null,3,'Jets Chairman Christopher Johnson Won’t Fine Players For Anthem Protests','<p>When it comes to New York Jets players kneeling during the national anthem, team chairman Christopher Johnson is a standup guy.</p>

	<p>Johnson said the team wouldn&rsquo;t fine any player for protesting, despite a new NFL owner-approved measure that enables teams to do so, according to reports.</p>

	<p>Wednesday&rsquo;s rule change says players must stand for the anthem or stay off the field. If any player kneels, his team would face a league fine. Teams can discipline individual players.</p>

	<p>&ldquo;As I have in the past, I will support our players wherever we land as a team,&rdquo; Johnson said in a statement, according to the New York Post. &ldquo;Our focus is not on imposing any Club rules, fines, or restrictions. Instead we will continue to work closely with our players to constructively advance social justice issues that are important to us. I remain extremely proud of how we demonstrated unity last season as well as our players&rsquo; commitment to strengthening our communities.&rdquo;</p>

	<p>Christopher Johnson locks arms with players during the national anthem before a game in 09tember 2017. AL BELLO VIA GETTY IMAGES Christopher Johnson locks arms with players during the national anthem before a game in 09tember 2017. Johnson elaborated on his pledge not to fine players in an interview with Newsday.</p>

	<p> &ldquo;I do not like imposing any club-specific rules,&rdquo; he said. &ldquo;If somebody (on the Jets) takes a knee, that fine will be borne by the organization, by me, not the players.</p>

	<p>&ldquo;I never want to put restrictions on the speech of our players. Do I prefer they stand? Of course. But I understand if they feel the need to protest. There are some big, complicated issues that we&rsquo;re all struggling with, and our players are on the front lines.&rdquo;</p>

	<p>Johnson&rsquo;s stance could get interesting in the team&rsquo;s hierarchy. He has been designated as the acting owner while his brother, team owner Woody Johnson, serves as President Donald Trump&rsquo;s ambassador to the United Kingdom, CBS Sports pointed out.</p>

	<p>Trump, who has repeatedly criticized demonstrating players, applauded the new NFL edict. &ldquo;You have to stand proudly for the national anthem or you shouldn&rsquo;t be playing,&rdquo; he said.</p>

	<p>The NFL Players Association criticized the league for failing to consult the union before the change, and vowed to carefully review the new policy.</p>','2021-12-16',null,1,null,'Ron Dicker',null);
INSERT INTO TBL_POST VALUES (null,4,'Facebook Accused Of Reading Texts And Accessing Microphones In Lawsuit','<p>Facebook stands accused of gathering substantially more information on users than it admits, according to claims laid out in court documents filed at the superior court in San Mateo, California, on behalf of a former startup, The Guardian first reported Thursday.</p>','24-05-18',null,1,null,'Sara Boboltz',null);
INSERT INTO TBL_POST VALUES (null,4,'14 Ways To Make Family Road Trips Easier, From Parents Who''ve Been There','<p>Family road trips can be a test for parents on how long they can keep kids occupied in the car before the children reach their breaking point, or more specifically, their tantrum point.</p>

	<p>We asked the HuffPost Parents community about their tips and tricks for keeping kids occupied during those long drives, and we&rsquo;ve got two words for you: baby wipes. Even if your kids aren&rsquo;t babies, our readers said.</p>

	<p>They also suggested other important items to always have in the car in case of emergencies and a few ways to keep the kids busy and happy. Here are 14 tips from our readers on how to make family road trips more manageable.</p>

	<p>&ldquo;We move and travel often and I&rsquo;ve learned a few tricks. If taking a road trip, consider covering your car&rsquo;s seats with a waterproof cover. It&rsquo;s much easier to clean, especially if your kids get carsick. It saves the upholstery from all kinds of messes: food, spills, vomit. I also recommend using Ziploc bags for carsick kids. That way you can seal it up and toss it without any spillage. Headphones are a lifesaver! I love Cozy Phones headphones for smaller kids. They are cute and totally wearable. And snacks are a must! Keep it simple and mess-free (or mostly mess-free) with fruit strips, applesauce pouches, jerky, etc. We like to keep a small soft-sided cooler in the car to store Lunchables and Go-Gurts. And no matter how old your kids are, bring baby wipes!&rdquo; &#8213; Heather Williams</p>

	<p>&ldquo;DVD player, iPad, another tablet, plenty of snacks, drinks and rest breaks for meals like breakfast or dinner. We usually eat sandwiches in the car. I pack an entire cooler full of sandwiches, fruit, cheese, and veggies likes carrot and celery sticks. I pack another cooler of water, juices boxes, tea for myself and Cokes for my husband. I make individual bags with a sandwich, fruit, and cheese. That way each person can grab what they want. My son is very easy to travel with, and as long as he&rsquo;s not hungry, sick or bored, he&rsquo;s good to go.&rdquo; &#8213; Melissa Watson</p>

	<p>&ldquo;I try to space things out, but it&rsquo;s a mix of each kid having screen time on their tablets, movies on our flip-down screen in the car, and snacks &#8213; lots of them. When we stop for gas/potty breaks, I let the kids get out to pick out a special treat at a gas station, even if it&rsquo;s candy. On our most recent trip I stocked up a cheap plastic bin with dry-erase boards for each kid with markers, crayons and coloring books. Wipes of any kind, whether baby wipes or Wet Ones in a canister are a huge help to clean messy hands from all the snacking. A full-sized bottle of hand sanitizer with a pump, a roll of paper towels, and box of tissue that I can keep within an arm&rsquo;s reach in the car, are also always on my necessities list.&rdquo; &#8213; Charlotte Kennedy</p>

	<p>&ldquo;For road trips we bring toys that are magnetic and a grabber toy... so kids strapped in car seats can often pick up the toys they drop without help from Mom or Dad!&rdquo; &#8213; Jenn Otto</p>

	<p>&ldquo;We have each kid pack a &lsquo;go bag.&rsquo; They have toys, books, headphones, tablets, and coloring books and colored pencils. We also get travel books and games from Usborne Books. We have a travel case for movies and a car DVD player, since we drive eight hours back to our hometown. Snacks and juice pouches are easily accessible, and [we sometimes use] &lsquo;calm-down candy&rsquo; as a bribe. I also have a container in the car for travel with Band-Aids, Pedialyte, Tylenol, Tums, wet wipes, and Neosporin.&rdquo; &#8213; Rachael Grayson</p>

	<p>&ldquo;First we start with car bingo ($1 cards at Target). When that gets boring we move to books, and when all else fails (and it usually does), it&rsquo;s screen time, baby! Lay on the iPhone, iPad, portable Blu-ray player! Works every time and gets us to where we are going with less bickering than when I was a kiddo.&rdquo; &#8213; Brianna Freeburn</p>

	<p>&ldquo;I like to visit the local comic book store and get some new comics for my boys. They&rsquo;re cheap and small enough for travel. They encourage reading and imagination. They also come in so many genres from &lsquo;My Little Pony&rsquo; to superheroes to everything Disney to Spongebob. Really can&rsquo;t go wrong.&rdquo; &#8213; Shannon Prieto</p>

	<p>&ldquo;We traveled from Ontario to British Columbia in Canada with four kids ages 1 to 7. I brought a bag of toys for each one, which they had finished looking at a half hour into the trip. We discovered they enjoyed looking out the windows most and playing car bingo games, despite them being very active kids. Our success also rode on our timing. We would pack up each morning and be on the road between 5 and 6 a.m., stop for gas, a pee break and to change clothes, then eat breakfast on the road again. We&rsquo;d stop for lunch at a park, but we&rsquo;d have finished eight hours of driving by early afternoon, so they had all afternoon and evening to play. It made it not seem like such long days when we got so much driving done while they were groggy!&rdquo; &#8213; Sarah Devries</p>

	<p>&ldquo;Some favorite toys/books and a couple new ones for emergencies. Lots of snacks. One or two containers of Play-Doh (keep it simple!), activity books and sticker books. Don&rsquo;t forget to play games. My kids are always distracted by playing I Spy, or when we are in the car finding certain colored cars or houses, or all the letters in the alphabet in order.&rdquo; &#8213; Amy Kilpatrick</p>

	<p>&ldquo;I pack magazines, coloring books and plain paper for drawing. Word search pads for our 8-year-old, and new coloring pens, as we travel by car a lot. We invested in a travel lap tray that wraps around her that has pockets and good edges so nothing can fall off. Oh, and not forgetting snacks!&rdquo; &#8213; Cheri Wickers</p>

	<p>&ldquo;Magnetic &lsquo;paper&rsquo; dolls are pretty cool and fun. They make lots of themes: pirates on a ship, animals on a farm, princess in a castle, etc.&rdquo; &#8213; Angela Ballard Peene</p>

	<p>&ldquo;With my son, Audible. He can handle long car rides only as long as he has a book to listen to. Works for me!&rdquo; &#8213; Cat Williams</p>

	<p>&ldquo;Whenever we go on road trips with the kiddos, we pack a portable DVD player, small toys, books, coloring books, drawing paper, and crayons/pencils. Also lots of snacks. I teach them games my brother and I played when we were younger and on the road. We talk about things we see out the windows, and I try to encourage talking about their books and what they mean to them.&rdquo; &#8213; Breanna Boenicke</p>

	<p>&ldquo;Snacks on snacks on snacks! Did I mention snacks? Also, suckers, Legos, some action figures that he forgot about having so it&rsquo;s like a new toy, and colorful pens/crayons to draw with. Play-Doh is nice as a last resort, but proceed with caution.&rdquo; &#8213; Crystal Michelle</p>','2021-12-16',null,1,null,'Taylor Pittman',null);
INSERT INTO TBL_POST VALUES (null,5,'Disney Reveals Opening Seasons For ''Star Wars'' Theme Park Lands','<p>Disney&rsquo;s &ldquo;Star Wars&rdquo;-themed lands at its California and Florida resorts now have opening seasons.</p>','2021-12-16',null,1,null,'Ed Mazza',null);
INSERT INTO TBL_POST VALUES (null,7,'Starbucks Says Anyone Can Now Sit In Its Cafes — Even Without Buying Anything','<p>Starbucks announced a new policy of inclusivity on Saturday that will permit anyone to sit in its cafes or use the stores&rsquo; restrooms, regardless of whether they&rsquo;ve bought anything, The Associated Press reported.</p>','2021-12-16',null,1,null,'Dominique Mosbergen',null);
INSERT INTO TBL_POST VALUES (null,6,'Self-Driving Uber In Fatal Accident Had 6 Seconds To React Before Crash','<p>The hardware on Uber&rsquo;s self-driving car that struck and killed a 49-year-old woman in Arizona this March worked just fine, a
	<p>That&rsquo;s one of several takeaways from a preliminary report released Thursday by the National Transportation Safety Board, which found Uber&rsquo;s self-driving Volvo SUV observed the woman six seconds before the fatal crash, but failed to take any action to avoid her.</p>

	<p>On-board software initially classified the pedestrian as an unknown object, then as a vehicle, and finally as a bicycle in the seconds leading up to the crash, though the vehicle couldn&rsquo;t definitively predict the bike&rsquo;s path and adjust accordingly.</p>

	<p>1.3 seconds before the collision, the SUV was traveling at about 40 mph and determined an &ldquo;emergency braking maneuver&rdquo; was necessary. No such action was taken, however, as the system is currently disabled &ldquo;to reduce the potential for erratic vehicle behavior,&rdquo; the NTSB said.</p>

	<p>Uber told HuffPost it disables the emergency braking system on all of its Volvo SUVs while the cars are operating in autonomous mode as it&rsquo;s a Volvo system, not an Uber system. That aligns with a report earlier this month that concluded the Uber did in fact recognize the woman, but failed to avoid her because of a software setting.</p>

	<p>A human safety driver who was in the Uber at the time should have intervened and taken control of the car, but didn&rsquo;t. Video from the accident shows the driver wasn&rsquo;t looking at the road, and instead was &ldquo;monitoring the self-driving system interface,&rdquo; she told NTSB investigators.</p>

	<p>Uber suspended all of its self-driving operations after the accident, and permanently ceased testing in Arizona on Wednesday. An Uber spokesperson told HuffPost that when it resumes testing in its two remaining cities of San Francisco and Pittsburgh, it will likely be on a more limited basis.</p>

	<p>Adding to the confusion, the median the pedestrian crossed prior to the collision features two prominent brick-colored pathways, seen below forming a broad &ldquo;X&rdquo;:</p>

	<p>Despite looking like a safe spot to cross, however, the paths aren&rsquo;t meant for pedestrians. A spokesperson for the City of Tempe declined to comment on the median&rsquo;s design or the intended purpose of the brick pathways, citing the ongoing investigation.</p>

	<p>The NTSB report also noted the pedestrian tested positive for methamphetamine and marijuana. That information is of limited use &#8213; the Uber should have identified and avoided her either way &#8213; but it does potentially provide insight into why she didn&rsquo;t cross at the crosswalk 360 feet to the north, and why she was unaware of the vehicle until immediately before impact.</p>

	<p>Investigators added the woman crossed in a section of road that isn&rsquo;t illuminated by street lights, was wearing dark clothing at the time, and her bicycle had no reflectors that were visible to the car.</p>

	<p>&ldquo;Over the course of the last two months, we&rsquo;ve worked closely with the NTSB,&rdquo; Uber said in an emailed statement to HuffPost Thursday. &ldquo;As their investigation continues, we&rsquo;ve initiated our own safety review of our self-driving vehicles program.&rdquo;</p>

	<p>The company noted it also hired former NTSB Chairman Christopher Hart earlier this month in an advisory role.</p>','2021-12-16',null,1,null,'Ryan Grenoble',null);
INSERT INTO TBL_POST VALUES (null,6,'Scientists Turn To DNA Technology To Search For Loch Ness Monster','<p>LONDON (Reuters) - A global team of scientists plans to scour the icy depths of Loch Ness next month using environmental DNA (eDNA) in an experiment that 05 discover whether Scotland&rsquo;s fabled monster really does, or did, exist.</p>','2021-12-16',null,1,null,null,null);
INSERT INTO TBL_POST VALUES (null,7,'Unusual Asteroid Could Be An Interstellar Guest To Our Solar System','<p>Researchers postulate that a peculiar asteroid spotted near Jupiter is a permanent guest from a completely different solar system.</p>','2021-12-16',null,1,null,'Carol Kuruvilla',null);
INSERT INTO TBL_POST VALUES (null,7,'Trump’s New ‘MAGA’-Themed Swimwear Sinks On Twitter','<p>President Donald Trump&rsquo;s online campaign store is now selling summer-themed items, including $65 beach towels and $55 &ldquo;Make American Great Again&rdquo; swimsuits.</p>','2021-12-16',null,1,null,'Ed Mazza',null);
INSERT INTO TBL_POST VALUES (null,8,'Seth Meyers Has 1 Funny Regret After Trump Cancels North Korea Summit','<p>Seth Meyers had such high hopes for his Donald Trump souvenir coin collection.</p>','2021-12-16',null,1,null,'Ron Dicker',null);
INSERT INTO TBL_POST VALUES (null,8,'Stage Door: Ute Lemper’s Songs From The Broken Heart, Confucius','<p>Ute Lemper, the acclaimed German chanteuse, bares her continental soul at the inviting 54 Below nightclub, downstairs from Studio 54, tonight and tomorrow. The great Kurt Weill interpreter is taking a departure from her acclaimed repertoire.</p>','2021-12-16',null,1,null,'Fern Siegel, ContributorDeputy Editor, MediaPost',null);
INSERT INTO TBL_POST VALUES (null,8,'California School First In The Nation To Be Digitally Mapped For First Responders','<p>Among the many disturbing truths that came to light following the 1999 massacre at Columbine High School was that it took Eric Harris and Dylan Klebold only 16 minutes to kill 13 people and wound 21 others. But it took police three hours and 14 minutes to find all of them.</p>','2021-12-16',null,1,null,'EdSource, Editorial Partner',null);

INSERT INTO POST_CATEGORY VALUES (null,1,5);
INSERT INTO POST_CATEGORY VALUES (null,2,3);
INSERT INTO POST_CATEGORY VALUES (null,3,15);
INSERT INTO POST_CATEGORY VALUES (null,4,8);
INSERT INTO POST_CATEGORY VALUES (null,5,15);
INSERT INTO POST_CATEGORY VALUES (null,6,16);
INSERT INTO POST_CATEGORY VALUES (null,7,13);
INSERT INTO POST_CATEGORY VALUES (null,8,14);
INSERT INTO POST_CATEGORY VALUES (null,9,5);
INSERT INTO POST_CATEGORY VALUES (null,10,16);
INSERT INTO POST_CATEGORY VALUES (null,11,14);
INSERT INTO POST_CATEGORY VALUES (null,12,14);
INSERT INTO POST_CATEGORY VALUES (null,13,5);
INSERT INTO POST_CATEGORY VALUES (null,14,7);
INSERT INTO POST_CATEGORY VALUES (null,15,3);
INSERT INTO POST_CATEGORY VALUES (null,16,2);

INSERT INTO TBL_COMMENT VALUES (null,2,1,'This project makes me happy to be a 30+ year Times subscriber... continue to innovate across all platforms, please.','2021-12-16',1,null,'comment',null);
INSERT INTO TBL_COMMENT VALUES (null,4,2,'Stunning photos and reportage. Infuriating that the Trump admistration''s draconian reinstatement of the global gag order will prevent men and women from receiving appropriate family planning advice, so obviously desperately   needed.','2021-12-16',1,null,'comment',null);
INSERT INTO TBL_COMMENT VALUES (null,5,3,'Brilliant work from conception to execution. I''ve never seen anything like it.  As a paper of record it is important to leave a record of these and other peoples for whom no one else speaks.  Please keep humanizing what are otherwise bland statistics.  It matters. Time and again their stories sounded like mine, could have been mine.  We''re about to see hordes of people far away experiencing utter devastation.  Empathy goes away unless there are names and stories, and pictures.  Links to helping at organizations, resources....  ','2021-12-16',1,null,'comment',null);
INSERT INTO TBL_COMMENT VALUES (null,6,4,'NYT reporters should provide a contributor''s link to a crowdfunding project or aid organization focused on the specific subject of news stories like this. Not for every sad story, but for the biggest and most obvious human problems on earth. <br/><br/>That should be a regular feature, with a dedicated click-button always located in the same familiar spot. Only 36% of America has truly hardened their hearts toward those suffering abroad. ','2021-12-16',1,null,'comment',null);
INSERT INTO TBL_COMMENT VALUES (null,7,5,'Could only have been done in print. Stunning. ','2021-12-16',1,null,'comment',null);
INSERT INTO TBL_COMMENT VALUES (null,8,1,'Thank you New York Times. People should be supporting and encouraging live theater which brings these photos to life. cab dramatists write about real issues?<br/>Money to arts not guns.','2021-12-16',1,null,'comment',null);
INSERT INTO TBL_COMMENT VALUES (null,9,2,'Proof that photojournalism is alive and well. Excellent work, Mr. Ferguson!','2021-12-16',1,null,'comment',null);
INSERT INTO TBL_COMMENT VALUES (null,10,3,'The OASIS Initiative, which I started with Prof. Malcolm Potts at UC Berkeley is focusing on building local leadership and the evidence base necessary to help people in the Sahel face Africa''s greatest development challenge: unprecedented population growth and effects of climate change in an already fragile region. Please consider supporting our work - the donate button is on the top right. Gifts to UC Berkeley have only about 3% indirect cost rate. <a href=http://www.oasisinitiative.berkeley.edu title=www.oasisinitiative.berkeley.edu target=_blank>www.oasisinitiative.berkeley.edu</a> ','2021-12-16',1,null,'comment',null);
INSERT INTO TBL_COMMENT VALUES (null,11,4,'I agree. I''ve just spent 30 minutes trying to find a place to donate money that will actually go to these people.','2021-12-16',1,null,'comment',null);
INSERT INTO TBL_COMMENT VALUES (null,12,5,'How about Katrina Pierson? Back to Palookaville for her!','2021-12-16',1,null,'comment',null);
INSERT INTO TBL_COMMENT VALUES (null,13,1,'You can fool some of the people all of the time. <br/><br/>Therein lies DJT''s political base. Day by day, that base, that pool, continues to shrink in number ... it''s just another way of draining the swamp. Keep working on that pool, DJT ... keep working on that pool. You can eliminate that swamp. Trust me ... I''m an expired notary.<br/><br/>Nobody does it better than you. You''re the greatest, DJT. Just keep on keeping on. You''ve got your stuff working.','2021-12-16',1,null,'comment',null);
INSERT INTO TBL_COMMENT VALUES (null,14,2,'The Dreaded Trump Curse?...you mean there''s only one???','2021-12-16',1,null,'comment',null);
INSERT INTO TBL_COMMENT VALUES (null,15,3,'No one is stopping Sean Spicer or any of the others from quitting if they are so horrified by Trump''s policies and practices.','2021-12-16',1,null,'comment',null);
INSERT INTO TBL_COMMENT VALUES (null,16,4,'Very good ','2021-12-16',1,null,'comment',null);
INSERT INTO TBL_COMMENT VALUES (null,17,5,'Oh my God, poor Sean Spicer. You wouldnâ€™t wish this on anyone.<br/><br/>Yes, yes you would!','2021-12-16',1,null,'reply',1);
INSERT INTO TBL_COMMENT VALUES (null,18,1,'Can''t we just get Sean Spicer, Ms. Conway and all of Trump''s other regular TV defenders--Hugh Hewitt et al.--before TV cameras for a daily 15 minute segment in which they all spout their deflections and misdirections without interruption? Why let reporters questions slow the flow of misinformation? Just let the deflectors spout it all out in a burst. Then we would not have to be plagued by their inanities throughout the day.<br/><br/>Fifteen minutes should be plenty of time for FOX News to get all the sound bites they need for any given day.<br/><br/>Fifteen minutes should also be plenty of time for those who wish to be misinformed to receive all the disinformation they could possibly want.','2021-12-16',1,null,'reply',1);
INSERT INTO TBL_COMMENT VALUES (null,19,2,'I keep waiting for Spicer to show at his podium wearing a beanie with a propeller on top.  I have to believe when he closes his eyes trying to sleep the inside of his brain is going like a pin ball machine.<br/>It would all be great material for a never to believed spoof on Washington if we weren''t watching our Nation circling the porcelain convenience.<br/>Curiouser and curiouser....','2021-12-16',1,null,'reply',6);
INSERT INTO TBL_COMMENT VALUES (null,5,3,'Bravo Gail !<br/>Add to your list of the pariah-like effect DT poses to  famous people - musicians, stars, artists, athletes, team owners, none of whom apparently want an invite to anything associated with this WH. (one exception - Bob Kraft- he was apparently needed at Mar-A -Lago when the nuclear football was being tossed around in the wake of the call from No. Korea.) ','2021-12-16',1,null,'reply',2);
INSERT INTO TBL_COMMENT VALUES (null,10,4,'What about poor Kellyanne? ','2021-12-16',1,null,'reply',3);
INSERT INTO TBL_COMMENT VALUES (null,5,5,'Oh yeah Gail we do wish this on everyone of these a holes.  They deserve what they are getting as does Don the Con who hopefully is going to get ousted to Siberia where he can ride off bareback with Putin into the Sunset on the back of a white horse.','2021-12-16',1,null,'reply',4);

INSERT INTO TBL_RATING VALUES (null,8,1,5,null);
INSERT INTO TBL_RATING VALUES (null,8,2,4,null);
INSERT INTO TBL_RATING VALUES (null,8,3,4,null);
INSERT INTO TBL_RATING VALUES (null,8,4,5,null);
INSERT INTO TBL_RATING VALUES (null,8,5,4,null);
INSERT INTO TBL_RATING VALUES (null,2,1,3,null);
INSERT INTO TBL_RATING VALUES (null,2,2,4,null);
INSERT INTO TBL_RATING VALUES (null,2,3,5,null);
INSERT INTO TBL_RATING VALUES (null,2,4,3,null);
INSERT INTO TBL_RATING VALUES (null,2,5,4,null);
INSERT INTO TBL_RATING VALUES (null,6,1,5,null);
INSERT INTO TBL_RATING VALUES (null,6,2,5,null);
INSERT INTO TBL_RATING VALUES (null,6,3,4,null);
INSERT INTO TBL_RATING VALUES (null,6,4,4,null);
INSERT INTO TBL_RATING VALUES (null,6,5,5,null);
INSERT INTO TBL_RATING VALUES (null,4,1,3,null);
INSERT INTO TBL_RATING VALUES (null,4,2,4,null);
INSERT INTO TBL_RATING VALUES (null,4,3,4,null);
INSERT INTO TBL_RATING VALUES (null,4,4,5,null);
INSERT INTO TBL_RATING VALUES (null,4,5,4,null);
INSERT INTO TBL_RATING VALUES (null,5,1,3,null);
INSERT INTO TBL_RATING VALUES (null,5,2,4,null);
INSERT INTO TBL_RATING VALUES (null,5,3,5,null);
INSERT INTO TBL_RATING VALUES (null,5,4,4,null);
INSERT INTO TBL_RATING VALUES (null,5,5,4,null);
INSERT INTO TBL_RATING VALUES (null,7,1,5,null);
INSERT INTO TBL_RATING VALUES (null,7,2,3,null);
INSERT INTO TBL_RATING VALUES (null,7,3,4,null);
INSERT INTO TBL_RATING VALUES (null,7,4,4,null);
INSERT INTO TBL_RATING VALUES (null,7,5,4,null);



