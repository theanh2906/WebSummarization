CREATE TABLE IF NOT EXISTS tbl_account (
    account_id      VARCHAR(100)    PRIMARY KEY NOT NULL ,
    username        VARCHAR(100)    NOT NULL ,
    password        VARCHAR(100)    NOT NULL ,
    role            VARCHAR(100)    NOT NULL ,
    is_active       BOOLEAN         NOT NULL ,
    create_time     TIMESTAMP
);

CREATE TABLE IF NOT EXISTS tbl_user (
    user_id         VARCHAR(100)    PRIMARY KEY NOT NULL ,
    account_id      VARCHAR(100)    NOT NULL ,
    fullname        VARCHAR(100)    NOT NULL ,
    dob             DATE,
    email           VARCHAR(100),
    address         VARCHAR(100),
    bio             VARCHAR(100),
    nationality     VARCHAR(100),
    avatar          TEXT
);

CREATE TABLE IF NOT EXISTS tbl_status (
    status_id       VARCHAR(100)    PRIMARY KEY NOT NULL ,
    status_name     VARCHAR(100)    NOT NULL
);

CREATE TABLE IF NOT EXISTS tbl_category (
    category_id     VARCHAR(100)    PRIMARY KEY NOT NULL ,
    category_name   VARCHAR(100)    NOT NULL,
    code            VARCHAR(100)    NOT NULL
);

CREATE TABLE IF NOT EXISTS tbl_rating (
    rating_id       VARCHAR(100)    PRIMARY KEY NOT NULL ,
    user_id         VARCHAR(100)    NOT NULL ,
    post_id         VARCHAR(100)    NOT NULL ,
    rating          NUMERIC         NOT NULL ,
    create_time     TIMESTAMP
);

CREATE TABLE IF NOT EXISTS tbl_post (
    post_id         VARCHAR(100)    PRIMARY KEY NOT NULL ,
    user_id         VARCHAR(100)    NOT NULL ,
    title           VARCHAR(100)    NOT NULL ,
    content         TEXT            NOT NULL ,
    create_time     TIMESTAMP       NOT NULL ,
    image           TEXT,
    status_id       VARCHAR(100)    NOT NULL ,
    overall_rating  NUMERIC(2,2),
    author          VARCHAR(100),
    abstract        VARCHAR(100),
    update_time     TIMESTAMP
);

CREATE TABLE IF NOT EXISTS tbl_comment (
    comment_id      VARCHAR(100)    PRIMARY KEY NOT NULL ,
    user_id         VARCHAR(100)    NOT NULL ,
    post_id         VARCHAR(100)    NOT NULL ,
    content         TEXT            NOT NULL ,
    create_time     TIMESTAMP       NOT NULL ,
    status          NUMERIC         NOT NULL ,
    update_time     TIMESTAMP       NOT NULL ,
    type            VARCHAR(100)    NOT NULL ,
    reply_to_comment    NUMERIC
);

CREATE TABLE IF NOT EXISTS post_category (
    post_id         VARCHAR(100)    NOT NULL ,
    category_id     VARCHAR(100)    NOT NULL ,
    PRIMARY KEY (post_id, category_id)
);

