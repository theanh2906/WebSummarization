package hcmiu.thesis.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "TBL_CATEGORY")
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Category {
	
	private static Category INSTANCE;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Integer categoryId;
	@Column
	private String categoryName;
	@Column
	private String code;
	
	public static Category getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Category();
		}
		return INSTANCE;
	}
	
}
