package hcmiu.thesis.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Input {
	private String textInput;
	private int sentenceNum;
	private String choice;
	
}
