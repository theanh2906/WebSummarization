package hcmiu.thesis.configurations;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import lombok.NoArgsConstructor;

@Configuration
//@EnableTransactionManagement
@PropertySource({ "classpath:application.properties"})
@EnableJpaRepositories("hcmiu.thesis.repositories")
@EntityScan("hcmiu.thesis.models")
@NoArgsConstructor
public class JpaConfig {
	
}