package hcmiu.thesis.utils.mappers;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import hcmiu.thesis.dtos.CategoryDTO;
import hcmiu.thesis.models.Category;

@Component
public class CategoryMapper {

	public static Category toModel(CategoryDTO dto) {
		final Category model = Category.getInstance();
		BeanUtils.copyProperties(dto, model);
		return model;
	}

	public static CategoryDTO toDto(Category model) {
		final CategoryDTO dto = CategoryDTO.getInstance();
		BeanUtils.copyProperties(model, dto);
		return dto;
	}
	
}
