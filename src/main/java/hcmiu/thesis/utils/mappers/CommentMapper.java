package hcmiu.thesis.utils.mappers;

import hcmiu.thesis.dtos.CommentDTO;
import hcmiu.thesis.models.Comment;
import org.springframework.beans.BeanUtils;

public class CommentMapper {

	public static Comment toModel(CommentDTO dto) {
		final Comment model = Comment.getInstance();
		BeanUtils.copyProperties(dto, model);
		return model;
	}

	public static CommentDTO toDto(Comment model) {
		final CommentDTO dto = CommentDTO.getInstance();
		BeanUtils.copyProperties(model, dto);
		return dto;
	}

}
