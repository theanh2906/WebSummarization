package hcmiu.thesis.utils.annotations.validators;

import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import hcmiu.thesis.models.User;
import hcmiu.thesis.repositories.UserRepository;

public class CheckEmailValidator implements ConstraintValidator<CheckEmail, String>{

	@Autowired
	UserRepository repo;
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value.isBlank()) {
			return true;
		} else {
		Optional<User> account = Optional.ofNullable(repo.findByEmail(value));
		return account.isEmpty() ? Boolean.TRUE : Boolean.FALSE;
		}
	}

}
