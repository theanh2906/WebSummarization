package hcmiu.thesis.utils;

import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.SimpleTokenizer;
import opennlp.tools.util.StringList;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WordUtil {
	public static int countWords (String text) {
		int total = 0;
		text = text.replaceAll("\\p{Punct}", "");
		StringList list = new StringList(SimpleTokenizer.INSTANCE.tokenize(text));
		total = list.size();
		return total;
	}

	public static String concatList(List<String> listWords) {
		return String.join(" ", listWords)
			.replaceAll(",","");
	}

	public static Integer countSentences(String text) throws IOException {
		File modelFile = Paths.get("models/en-sent.bin").toFile();
		text = text.replaceAll("([A-Z])\\.", "$1");
		InputStream is = new FileInputStream(modelFile);
		SentenceModel model = new SentenceModel(is);
		SentenceDetectorME sd = new SentenceDetectorME(model);
		return sd.sentDetect(text).length;
	}

	public static List<String> listSentences (String text) throws IOException{
		File modelFile = Paths.get("models/en-sent.bin").toFile();
		text = text.replaceAll("([A-Z])\\.", "$1");
		InputStream is = new FileInputStream(modelFile);
		SentenceModel model = new SentenceModel(is);
		SentenceDetectorME sd = new SentenceDetectorME(model);
		return Arrays.asList(sd.sentDetect(text));
	}

	public static Integer countSameSentences (String textA, String textB) throws IOException{
		List<String> sameSentences = new ArrayList<>();
		List<String> listA = listSentences(textA);
		List<String> listB = listSentences(textB);
		listA.stream().filter(each -> listB.contains(each)).forEach(sameSentence -> sameSentences.add(sameSentence));
		return sameSentences.size();
	}
	
}
