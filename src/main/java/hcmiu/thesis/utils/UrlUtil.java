package hcmiu.thesis.utils;

import hcmiu.thesis.commons.EmailTypes;
import hcmiu.thesis.dtos.RegisterInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
public class UrlUtil {

	@Value("${base-root}")
	private static String HOME_ROOT;
	public static String createLink (EmailTypes emailTypes, RegisterInfo info) {
		String link = null;
		
		switch (emailTypes) {
		case SUCCESSFUL_REGISTRATION:
			link = "http://web-demo.top/summarization/confirmed?username=" + info.getUsername();

		default:
			break;
		}
		
		return link;
	}
}
