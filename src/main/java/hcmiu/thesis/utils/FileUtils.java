package hcmiu.thesis.utils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class FileUtils {

	enum WriteMethod {
		OVERRIDE,
		SKIP_ON_EXIST
	}

	public static String concatStringList(List<String> listWords) {
		return String.join(" ", listWords)
				.replaceAll(",","");
	}

	public static List<String> readFileAsList (File file) {
		ArrayList<String> list = new ArrayList<>();
		try (FileReader f = new FileReader(file)){
			StringBuffer sb = new StringBuffer();
			while (f.ready()) {
				char c = (char) f.read();
				if (c == '\n') {
					list.add(sb.toString());
					sb = new StringBuffer();
				} else sb.append(c);
			    if (sb.length() > 0) {
			        list.add(sb.toString());
			    }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public static String readFileAsString (File file) {
		return concatStringList(readFileAsList(file));
	}

    public void toTextFile(String text, String fileName, WriteMethod method) throws IOException {
        File output = new File(String.format("./%s.txt", fileName));
        switch (method) {
			case OVERRIDE:
				if (output.exists()) {
					output.delete();
					FileWriter write = new FileWriter(String.format("./%s.txt", fileName));
					write.write(text);
					write.close();
				} else {
					FileWriter write = new FileWriter(String.format("./%s.txt", fileName));
					write.write(text);
					write.close();
				}
			case SKIP_ON_EXIST:
				if (output.exists()) {
					throw new FileAlreadyExistsException("File already exist!");
				} else {
					FileWriter write = new FileWriter(String.format("./%s.txt", fileName));
					write.write(text);
					write.close();
				}
		}

    }

	public void toFile(String text, String fileNameWithExtension, WriteMethod method) throws IOException {
		File output = new File(String.format("./%s", fileNameWithExtension));
		switch (method) {
			case OVERRIDE:
				if (output.exists()) {
					output.delete();
					FileWriter write = new FileWriter(String.format("./%s.txt", fileNameWithExtension));
					write.write(text);
					write.close();
				} else {
					FileWriter write = new FileWriter(String.format("./%s.txt", fileNameWithExtension));
					write.write(text);
					write.close();
				}
			case SKIP_ON_EXIST:
				if (output.exists()) {
					throw new FileAlreadyExistsException("File already exist!");
				} else {
					FileWriter write = new FileWriter(String.format("./%s.txt", fileNameWithExtension));
					write.write(text);
					write.close();
				}
		}
	}

	public static String readFile(File file) throws IOException {
		String text = "";
		Scanner scan = new Scanner(file);
		while(scan.hasNextLine()) {
			text += scan.nextLine()+"\n";
		}
		return text;
	}

//	public static List<List<String>> readFileAsList(String filePath) throws IOException {
//
//	}

}
