package hcmiu.thesis.utils;

import hcmiu.thesis.commons.FileExtensions;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CSVUtils {


    public static CSVUtils getInstance() {
        return new CSVUtils();
    }
//    public void toCSV(Iterable<String> content ,String fileName) throws IOException{
//        BufferedWriter writer = Files.newBufferedWriter(Paths.get(fileName.concat(FileExtensions.CSV.value())));
//        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(Headers.class));
//        csvPrinter.printRecord(content);
//        csvPrinter.flush();
//    }

    public static CSVPrinter createCSV(String fileName, String ... headers) throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(Paths.get(fileName.concat(FileExtensions.CSV.value())));
        return new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(headers));
    }

    public static BufferedWriter prepareWriter(String fileName) throws IOException{
        return Files.newBufferedWriter(Paths.get(fileName.concat(FileExtensions.CSV.value())));
    }

    public static CSVParser readCSV(String pathToCSV) throws IOException{
        Reader reader = Files.newBufferedReader(Paths.get(pathToCSV));
        return new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader()
            .withIgnoreHeaderCase()
            .withTrim());
    }

    public static String formatURL(String url) {
        return url.replaceAll("\\s+", "-");
    }
}
