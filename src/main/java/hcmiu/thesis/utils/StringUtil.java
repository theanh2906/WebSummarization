package hcmiu.thesis.utils;

import com.sun.faces.util.CollectionsUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringUtil {
	
	private static final BCryptPasswordEncoder encode = new BCryptPasswordEncoder(12);
	
    public static List<Integer> getDigitsFromQuery(final String str) {
        if (str.isEmpty()) {
            return Collections.emptyList();
        }
        final int sz = str.length();
        final StringBuilder strDigits = new StringBuilder(sz);
        for (int i = 0; i < sz; i++) {
            final char tempChar = str.charAt(i);
            if (Character.isDigit(tempChar)) {
                strDigits.append(tempChar);
            }
        }
        List<Integer> listInt = new ArrayList<>();
        String[] strArr = strDigits.toString().split("");
        for (String each : strArr) {
        	listInt.add(Integer.parseInt(each));
        }
        return listInt;
    }
    
    public static String encode(String string) {
    	return encode.encode(string);
    }
    
    public static String generatePassword () {
    	return RandomStringUtils.randomAlphanumeric(10);
    }

    public static String uppercaseFirstLetter(String word) {
        String output ="";
        String words[] = word.trim().split("\\s+");
        for (String each : words) {
            output = output.concat(each.substring(0,1).toUpperCase().concat(each.substring(1))).concat(" ");
        }
        return output.trim();
    }
}
