package hcmiu.thesis.repositories;

import hcmiu.thesis.commons.PostQuery;
import hcmiu.thesis.commons.PostStatus;
import hcmiu.thesis.dtos.CommentDTO;
import hcmiu.thesis.dtos.PostDTO;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EntityManagerRepository {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<PostDTO> getAllPosts() {
		Query query = entityManager.createQuery(PostQuery.GET_ALL_POSTS, PostDTO.class);
		return query.getResultList();
	}
	
	public PostDTO getPostById (Integer id) {
		return getAllPostsByStatus(PostStatus.ACTIVE.getValue())
			.stream()
			.filter(each -> each.getPostId()
				.equals(id)).findFirst()
			.get();
	}
	
	@SuppressWarnings("unchecked")
	public List<PostDTO> getAllPostsByStatus(Integer status) {
		Query query = entityManager.createQuery(PostQuery.GET_ALL_POSTS_BY_STATUS, PostDTO.class);
		return query.setParameter(1, status).getResultList();
	}
	
	public List<PostDTO> getAllPostsByCategory(String category ) {
		return getAllPostsByStatus(PostStatus.ACTIVE.getValue())
			.stream()
			.filter(each -> each.getCategoryName()
				.equals(category)).collect(Collectors.toList());
	}

	public List<PostDTO> findPostByText(String text) {
		Query query = entityManager.createQuery(PostQuery.SEARCH_POST_BY_TEXT, PostDTO.class);
		query.setParameter("text", "%" + text.strip().toLowerCase() + "%");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<CommentDTO> getAllComments () {
		Query query = entityManager.createQuery(PostQuery.GET_ALL_COMMENTS, CommentDTO.class);
		return query.getResultList();
	}


}
