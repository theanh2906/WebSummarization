package hcmiu.thesis.repositories.impl;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class POSTagger {
    public static List<String> nouns(String text) {
        List<String> nounsList = new ArrayList<>();
        List<String> nounTags = Arrays.asList("NN", "NNS", "NNPS", "NNP");
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
        CoreDocument document = pipeline.processToCoreDocument(text);
        for (CoreLabel tok : document.tokens()) {
            if(nounTags.contains(tok.tag())) {
                nounsList.add(tok.word());
            }
        }
        return nounsList;
    }

}
