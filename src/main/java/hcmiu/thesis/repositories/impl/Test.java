package hcmiu.thesis.repositories.impl;

import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        List<String> listA = Arrays.asList("1", "2", "3");
        List<String> listB = Arrays.asList("1", "2", "5", "6", "7");
        List<String> listC = new ArrayList<>();

        Flux<String> fluxA = Flux.fromIterable(listA);
        Flux<String> fluxB = Flux.fromIterable(listB);
//        fluxA.collectList().map(strings -> listB.remove(strings));

//        listB = listB.stream().filter(s -> !listA.contains(s)).collect(Collectors.toList());
        fluxA
                .collectList()
                .flatMap(aElems -> fluxB
                        .filter(s -> !aElems.contains(s))
                        .collectList())
                .doOnNext(listSignal -> listC.addAll(listSignal))
                .subscribe();
//        Flux.fromIterable(listC).doOnEach(stringSignal -> System.out.println(stringSignal.get()));
        listC.forEach(System.out::println);
    }
}
