package hcmiu.thesis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hcmiu.thesis.models.PostCategory;

@Repository
public interface PostCategoryRepository extends CrudRepository<PostCategory, Integer>, JpaRepository<PostCategory, Integer>{
	
}
