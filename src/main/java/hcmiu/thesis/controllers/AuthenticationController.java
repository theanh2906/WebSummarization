package hcmiu.thesis.controllers;

import hcmiu.thesis.commons.MessageStatus;
import hcmiu.thesis.commons.SystemMessage;
import hcmiu.thesis.dtos.RegisterInfo;
import hcmiu.thesis.services.AdminService;
import hcmiu.thesis.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class AuthenticationController {
	
	@Autowired
	AuthenticationService authService;
	
	@Autowired
	AdminService adminService;
	
	@GetMapping("/login")
	public String showLogin () {
		return "components/login";
	}
	
	@GetMapping("/register")
	public ModelAndView showRegistrationForm () {
		ModelAndView mav = new ModelAndView("components/register");
		mav.addObject("input", new RegisterInfo());
		return mav;
	}
	
	@PostMapping("/register")
	public String signup(@Valid @ModelAttribute("input") RegisterInfo info, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			model.addAttribute("message", SystemMessage.BINDING_ERROR);
			return "components/register";
		}
		
		authService.register(info);
		model.addAttribute("status", MessageStatus.WARNING.getValue());
		model.addAttribute("responseMessage", SystemMessage.REGISTRATION_WARNING);
		return "home";
	}
	
	@GetMapping("/confirmed")
	public String changePassword (@RequestParam("username") String username) {
		adminService.changeAccountStatus(username, Boolean.TRUE);
		return "components/change-pass";
	}
	
	
}
