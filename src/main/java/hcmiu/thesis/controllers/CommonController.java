package hcmiu.thesis.controllers;

import hcmiu.thesis.commons.WebConstant;
import hcmiu.thesis.dtos.*;
import hcmiu.thesis.services.CategoryService;
import hcmiu.thesis.services.CommentService;
import hcmiu.thesis.services.PostService;
import hcmiu.thesis.utils.CollectionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@ControllerAdvice
public class CommonController {

	@Value("${latest-article-size}")
	private Integer articleSize;

	@Value("${latest-comment-size}")
	private Integer commentSize;

	@Autowired
	private PostService postService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private CommentService commentService;

	@ModelAttribute
	public void addAttribute(ModelMap model) {
		Function<List<PostDTO>, List<YearDTO>> allYears = (listArticle) -> {
			List<YearDTO> listYears = new ArrayList<>();
			List<Integer> years = listArticle
			.stream()
			.map(each -> each.getCreateTime().getYear())
			.distinct()
			.sorted(CollectionUtil.SORT_DESC)
			.collect(Collectors.toList());
			years.forEach(year -> {
				listYears.add(new YearDTO(year, listArticle
				.stream()
				.filter(p -> year.equals(p.getCreateTime().getYear()))
				.collect(Collectors.toList())
				.size()));
			});
			return listYears;
		};

		List<CategoryDTO> listCategories = categoryService.getAllCategories();
		List<PostDTO> listArticle = postService.getAllPostsByStatus(1);
		List<YearDTO> listYears = allYears.apply(listArticle);
		List<String> categories = listArticle
				.stream()
				.map(PostDTO::getCategoryName)
				.distinct()
				.collect(Collectors.toList());

		List<CategoryCountDTO> allCategories = categories
				.stream()
				.map(each -> new CategoryCountDTO(each, postService.getAllPostsByCategory(1, each)
					.size()))
				.collect(Collectors.toList());

		List<PostDTO> latestNews = postService
				.getAllPostsByStatus(1)
				.stream()
				.sorted((a, b) -> b.getCreateTime().compareTo(a.getCreateTime()))
				.collect(Collectors.toList())
				.subList(0, articleSize-1);

		model.addAttribute("listCategories", listCategories);
		model.addAttribute("allCategories", allCategories);
		model.addAttribute("listYears", listYears);
		model.addAttribute("headerTitle", WebConstant.WEBSITE_NAME);
		model.addAttribute("latestNews", latestNews);
		List<CommentDTO> latestComments = commentService.getAllComments().subList(0, commentSize-1);
		model.addAttribute("latestComments", latestComments);
	}

	@ModelAttribute
	public void addCommentAttribute(ModelMap model) {

	}

}
