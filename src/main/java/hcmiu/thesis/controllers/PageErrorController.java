package hcmiu.thesis.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageErrorController implements ErrorController {
	
	@GetMapping("/403")
	public String accessDenied (ModelMap model) {
		model.addAttribute("title", "Access Denied");
		model.addAttribute("errorCode", "403");
		model.addAttribute("errorCodeName", "Access Denied!");
		model.addAttribute("detail", "You don't have permission to accces this page. Please contact page admin!!");
		return "error";
	}
	
	@GetMapping("/error")
	public String pageNotFound (ModelMap model) {
		model.addAttribute("title", "Page Not Found");
		model.addAttribute("errorCode", "404");
		model.addAttribute("errorCodeName", "Page Not Found!");
		model.addAttribute("detail", "Something went wrong!");
		return "error";
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}
}
