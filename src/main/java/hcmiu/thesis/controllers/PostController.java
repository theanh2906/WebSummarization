package hcmiu.thesis.controllers;

import hcmiu.thesis.commons.MessageStatus;
import hcmiu.thesis.commons.PostStatus;
import hcmiu.thesis.commons.SystemMessage;
import hcmiu.thesis.dtos.CommentDTO;
import hcmiu.thesis.dtos.CreatePostDTO;
import hcmiu.thesis.dtos.DemoDTO;
import hcmiu.thesis.dtos.PostDTO;
import hcmiu.thesis.models.Input;
import hcmiu.thesis.models.PostCategory;
import hcmiu.thesis.models.User;
import hcmiu.thesis.services.*;
import hcmiu.thesis.utils.CollectionUtil;
import hcmiu.thesis.utils.WordUtil;
import hcmiu.thesis.utils.mappers.PostMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class PostController {
	@Autowired
	private PostService postService;
	
	@Autowired
	private RatingService ratingService;
	
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private ProfileService profileService;
	
	@Autowired
	private AuthenticationService authService;
	
	
	@GetMapping("/new")
	public ModelAndView viewForm () {
		return new ModelAndView("home","input", new Input());
	}
	
	@GetMapping("/header")
	public ModelAndView viewNav() {
		return new ModelAndView("components/header");
	}
	
	@GetMapping("/footer")
	public ModelAndView viewFooter() {
		return new ModelAndView("components/footer");
	}
	
	@GetMapping({"/home", ""})
	public ModelAndView viewBlog(@RequestParam(required = false) String category,
								 @RequestParam(required = false) Integer year,
								 @RequestParam(required = false) String text) {
		ModelAndView mav = new ModelAndView();
		Optional<String> checkCategory = Optional.ofNullable(category);
		Optional<Integer> checkYear = Optional.ofNullable(year);
		Optional<String> checkText = Optional.ofNullable(text);
		List<PostDTO> listPost = new ArrayList<>();
		if (checkCategory.isEmpty() && checkYear.isEmpty() && checkText.isEmpty()) {
			listPost = postService.getAllPostsByStatus(1);
		} else if (checkYear.isPresent() && checkCategory.isEmpty() && checkText.isEmpty()){
			listPost = postService.getAllPostsByYear(PostStatus.ACTIVE.getValue(), year);
			mav.addObject("message","Year: "+ year);
		} else if (checkYear.isEmpty() && checkCategory.isPresent() && checkText.isEmpty()){
			listPost = postService.getAllPostsByCategory(PostStatus.ACTIVE.getValue(), category);
			mav.addObject("message","Category: "+ category);
		} else if (checkYear.isEmpty() && checkCategory.isEmpty() && checkText.isPresent()){
			listPost = postService.findPostsByText(checkText.get());
			mav.addObject("message", "Keyword: " + checkText.get());
		}
		
		mav.setViewName("home");
		CollectionUtil.sortDateDecs(listPost);
		mav.addObject("listPost",listPost);
		return mav;
	}
	
	@GetMapping({"/post/{id}", "/authenticated/post/{id}"})
	public ModelAndView viewPost(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("post");
		PostDTO post = postService.getPostById(id);
		List<PostDTO> relatedPosts = postService.getAllPostsByStatus(PostStatus.ACTIVE.getValue())
				.stream()
				.filter(postDTO -> postDTO.getCategoryName().equals(post.getCategoryName()))
				.collect(Collectors.toList());
		if (relatedPosts.size() < 5) {
			relatedPosts = relatedPosts.subList(0, relatedPosts.size());
		} else {
			relatedPosts = relatedPosts.subList(0, 5);
		}
		mav.addObject("listComments", commentService.getCommentsDtoByPostId(id));
		mav.addObject("listRating", ratingService.getRatingsByPostIdFilterByStar(id));
		mav.addObject("listPercentage", ratingService.getPercentageOfRating(ratingService.getRatingsByPostIdFilterByStar(id)));
		mav.addObject("post", post);
		mav.addObject("relatedPosts", relatedPosts);
		return mav;
	}
	
	@GetMapping("/create-post")
	public ModelAndView createPostForm() {
		ModelAndView mav = new ModelAndView();
		mav.addObject("headerTitle", "Create new post");
		mav.setViewName("create-post");
		mav.addObject("input", new CreatePostDTO());
		return mav;
	}
	
	@PostMapping("/create-post")
	public String testResponse(@Valid @ModelAttribute("input") CreatePostDTO body, BindingResult result, ModelMap model, Authentication auth) {
		if (result.hasErrors()) {
			return "create-post";
		}
		body.setCreateTime(LocalDateTime.now());
		body.setUserId(authService.findUserIdByUsername(auth.getName()));
		if (body.getAuthor().isBlank()) {
			body.setAuthor(auth.getName());
		}
		PostCategory postCategory = PostCategory.getInstance();
		BeanUtils.copyProperties(body, postCategory);
		Boolean isSuccessful = postService.createPost(PostMapper.toModel(body), postCategory);
		model.addAttribute("responseMessage", isSuccessful ? SystemMessage.SUCCESSFUL_CREATE_POST : SystemMessage.FAIL_CREATE_POST);
		model.addAttribute("status", isSuccessful ? MessageStatus.SUCCESS.getValue() : MessageStatus.ERROR.getValue());
		return  isSuccessful ? "home" : "create-post";
	}
	
	@GetMapping("/rating")
	public ModelAndView viewRating() {
		return new ModelAndView("components/Rating");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/update")
	@ResponseBody
	public ResponseEntity<CommentDTO> test(@RequestParam Integer id) {
		return new ResponseEntity(commentService.getCommentsDtoByPostId(id), HttpStatus.OK);
	}
	
	@GetMapping("/profile")
	public ModelAndView showProfile(Authentication auth) {
		ModelAndView mav = new ModelAndView("profile");
		User profile = profileService.getPersonalInfoByUsername(auth.getName());
		List<PostDTO> listPosts = postService.getAllPosts()
		.stream()
		.filter(each -> each.getUserId()
			.equals(profile.getUserId()) && each.getStatusName().equals("Active"))
		.collect(Collectors.toList());
		mav.addObject("profile", profile);
		mav.addObject("numberOfPosts", postService.getAllPostsByUsername(auth.getName()).size());
		mav.addObject("listPosts", listPosts);
		return mav;
	}
	
	@PostMapping("/delete")
	public String deletePosts (@RequestBody String raw) {
		List<Integer> listInt = new ArrayList<>();
		String[] arr = raw.replace("index=", "").replace("&", "").split("");
		for(String e : arr) {
			listInt.add(Integer.parseInt(e));
		}
		postService.changePostStatusById(listInt, 2);
		return "redirect:profile";
	}
	
	@GetMapping("/update-abstract")
	@ResponseBody
	public String updateAbstract() {
		postService.updateAbstract();
		return "Updated";
	}

	@GetMapping("/carousel")
	public ModelAndView viewJumbo () {
		return new ModelAndView("components/carousel");
	}

	@GetMapping("/demo")
	public String showDemo(ModelMap model) {
		model.addAttribute("input", new DemoDTO());
		return "demo";
	}

	@PostMapping("/demo/submit")
	public String showResult(@ModelAttribute("input") DemoDTO body, ModelMap model) {
		String summary;
		if (body.getMethod().equals("wf")) {
			summary = postService.getSummary(body.getTextInput(), body.getSentenceNum());
		} else {
			summary = postService.getLSA(body.getTextInput(), body.getSentenceNum());
		}
		int totalCount = WordUtil.countWords(body.getTextInput());
		int summaryCount = WordUtil.countWords(summary);
		double percentage = (double)summaryCount / (double)totalCount * 100;
		String stat = "_ Summary Count: "+summaryCount+System.getProperty("line.separator")+
				"_ Total Count: "+totalCount+System.getProperty("line.separator")+
				"_ Percentage: "+percentage;
		model.addAttribute("stat", stat);
		model.addAttribute("summary", summary);
		return "demo";
	}
}
