package hcmiu.thesis.controllers;

import hcmiu.thesis.dtos.DemoDTO;
import hcmiu.thesis.dtos.PostDTO;
import hcmiu.thesis.repositories.SummarizationDao;
import hcmiu.thesis.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
public class RestApiController {
	
	@Autowired
	private PostService postService;

	@Autowired
	private PostService summarization;
	
	@GetMapping("/categories")
	public List<String> allCategories () {
		return postService.getAllPosts().stream().map(PostDTO::getCategoryName).collect(Collectors.toList());
	}

	@PostMapping("/text")
	@ResponseBody
	public String getSummary(@RequestBody DemoDTO input) {
		return summarization.getSummary(input.getTextInput(), input.getSentenceNum());
	}
}
