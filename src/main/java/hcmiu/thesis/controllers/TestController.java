package hcmiu.thesis.controllers;

import hcmiu.thesis.dtos.CommentDTO;
import hcmiu.thesis.dtos.CreatePostDTO;
import hcmiu.thesis.dtos.PostDTO;
import hcmiu.thesis.models.Post;
import hcmiu.thesis.models.User;
import hcmiu.thesis.repositories.AccountReposity;
import hcmiu.thesis.repositories.EntityManagerRepository;
import hcmiu.thesis.repositories.impl.Summarizer;
import hcmiu.thesis.services.*;
import hcmiu.thesis.utils.StringUtil;
import hcmiu.thesis.utils.WebScaping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/test")
public class TestController {
	
	@Autowired
	EntityManagerRepository entityManager;
	
	@Autowired
	ProfileService profileService;
	
	@Autowired
	AccountReposity accountRepo;
	
	@Autowired
	PostService postService;
	
	@Autowired
	RatingService ratingService;
	
	@Autowired
	JavaMailSender emailSender;
	
	@Autowired
	MailService mailService;
	
	@Autowired
	PostService summarize;
	
	@Autowired
	CommentService commentService;
	
	@GetMapping("/{cat}")
	@ResponseBody
	public List<PostDTO> getCategory(@PathVariable String cat) {
		return entityManager.getAllPostsByCategory(cat);
	}
	
	@GetMapping("/username/{username}")
	@ResponseBody
	public User getUserProfile (@PathVariable String username) {
		return profileService.getPersonalInfoByUsername(username);
	}

	@GetMapping("/posts")
	@ResponseBody
	public List<Post> getAllPosts() {
		List<Post> postList = postService.getListOfPosts();
		System.out.println(postList.size());
		return postList;
	}
	
	@PostMapping("/delete_post/{id}")
	@ResponseBody
	public String deletePost (@PathVariable("id") Integer id) {
		return postService.deletePostById(id);
	}
	
	@GetMapping("/overall/{id}")
	@ResponseBody
	public Double calculateOverall (@PathVariable Integer id) {
		return ratingService.calculateOverall(id);
	}
	
	@GetMapping("/update_overall")
	@ResponseBody
	public List<Post> updateOverall () {
		return ratingService.updateOverall();
	}
	
	@PostMapping("/multiple-delete")
	@ResponseBody
	public List<Integer> getMultiple (@RequestBody String raw) {
		List<Integer> listInt = new ArrayList<>();
		String[] arr = raw.replace("index=", "").replace("&", "").split("");
		for(String e : arr) {
			listInt.add(Integer.parseInt(e));
		}
		return listInt;
	}

	@GetMapping("/login-page")
	public ModelAndView showLoginForm() {
		return new ModelAndView("/components/login");
	}
	
	@RequestMapping("/show-comment/{id}")
	@ResponseBody
	public List<CommentDTO> showComment(@PathVariable Integer id) {
		return commentService.getCommentsDtoByPostId(id);
	}
	
	@GetMapping("/test-view")
	public ModelAndView showView() {
		return new ModelAndView("admin/test");
	}
	
	@GetMapping("/text")
	public String demo() {
		return WebScaping.scaping("https://abcnews.go.com/Politics/live-updates/2020-election-vote-results-court-transition/?id=74449971");
	}
	
	@PostMapping("/abstracts")
	@ResponseBody
	public String genAbstract(@ModelAttribute CreatePostDTO body) {
		return summarize.getSummary(body.getContent(), 3);
	}

	@GetMapping("/test-search")
	@ResponseBody
	public List<PostDTO> testFind(@RequestParam("text") String text) {
		return postService.findPostsByText(text);
	}

	@PostMapping(value = "/test-create-comment", produces = "application/json")
	public @ResponseBody String createCommentTestResponse(@RequestBody CommentDTO body) {
		return body.toString();
	}

	@PostMapping("/doc-cat")
	@ResponseBody
	public List<String> recommendCategory(@RequestBody String text) throws IOException {
		return Summarizer.getCategory(text)
			.stream()
			.map(StringUtil::uppercaseFirstLetter)
			.collect(Collectors.toList());
	}
}
