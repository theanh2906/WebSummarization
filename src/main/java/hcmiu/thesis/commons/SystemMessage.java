package hcmiu.thesis.commons;

public class SystemMessage {
	
	public static final String USER_NOT_FOUND = "Error! User not found!";
	public static final String SUCCESSFUL_DELETE = "Successfully delete!";
	public static final String POST_NOT_FOUND = "Error! Post not found!";
	public static final String BINDING_ERROR = "There was an error occured!";
	public static final String USERNAME_ALREADY_EXIST = "Username is already existed. Please input again!";
	public static final String EMAIL_ALREADY_EXIST = "Email is already existed. Please input again!";
	public static final String REGISTRATION_WARNING = "Please check your mailbox for confirmation link";
	public static final String SUCCESSFUL_CREATE_POST = "Successfully create post!";
	public static final String FAIL_CREATE_POST = "An error happen while creating post!";
}
