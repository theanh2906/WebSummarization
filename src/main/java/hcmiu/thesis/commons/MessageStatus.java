package hcmiu.thesis.commons;

public enum MessageStatus {
	SUCCESS("success"),
	ERROR("alert"),
	WARNING("warning");
	String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	MessageStatus(String value) {
		this.value = value;
	}
}
