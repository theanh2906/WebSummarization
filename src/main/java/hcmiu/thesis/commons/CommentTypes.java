package hcmiu.thesis.commons;

public enum CommentTypes {
    COMMENT("comment"),
    REPLY("reply");

    String value;

    CommentTypes(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
