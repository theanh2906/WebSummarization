package hcmiu.thesis.commons;

public enum FileExtensions {
    TXT(".txt"),
    CSV(".csv");

    String value;

    FileExtensions(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
