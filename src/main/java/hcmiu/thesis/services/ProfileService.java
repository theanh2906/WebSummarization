package hcmiu.thesis.services;

import hcmiu.thesis.models.User;

public interface ProfileService {
	User getPersonalInfo (Integer userId);

	User getPersonalInfoByUsername(String username); 
}
