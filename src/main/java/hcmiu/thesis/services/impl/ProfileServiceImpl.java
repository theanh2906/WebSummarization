package hcmiu.thesis.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hcmiu.thesis.models.User;
import hcmiu.thesis.repositories.AccountReposity;
import hcmiu.thesis.repositories.ProfileRepository;
import hcmiu.thesis.services.ProfileService;

@Service
public class ProfileServiceImpl implements ProfileService {

	@Autowired
	ProfileRepository profileRepo;
	@Autowired
	AccountReposity accountRepo;
	
	@Override
	public User getPersonalInfo(Integer accountId) {
		// TODO Auto-generated method stub
		Optional<User> checkNullProfile = Optional.ofNullable(profileRepo.findByAccountId(accountId));
		return checkNullProfile.isPresent() ? profileRepo.findByAccountId(accountId) : new User();
	}
	
	@Override
	public User getPersonalInfoByUsername (String username) {
		Integer accountId = accountRepo.findAll().stream().filter(each -> each.getUsername().equals(username)).findFirst().get().getAccountId();
		return getPersonalInfo(accountId);
	}

}
