package hcmiu.thesis.services.impl;

import hcmiu.thesis.commons.PostStatus;
import hcmiu.thesis.models.Account;
import hcmiu.thesis.models.Post;
import hcmiu.thesis.repositories.AccountReposity;
import hcmiu.thesis.repositories.PostRepository;
import hcmiu.thesis.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService{
	
	@Autowired
	private AccountReposity accountRepository;

	@Autowired
	private PostRepository postRepository;

	@Override
	public Account changeAccountStatus(Integer id, Boolean isActive) {
		Account account = accountRepository.findById(id).get();
		account.setIsActive(isActive);
		return accountRepository.save(account);
	}

	@Override
	public List<Account> getAllAccounts() {
		return accountRepository.findAll();
	}

	@Override
	public void deleteAccount(Integer id) {
		accountRepository.deleteById(id);
	}

	@Override
	public Account changeAccountStatus(String username, Boolean isActive) {
		Account account = accountRepository.findByUsername(username);
		account.setIsActive(isActive);
		return accountRepository.save(account);
	}

	@Override
	public Post changePostStatus(Integer id, Boolean isActive) {
		Post post = postRepository.findById(id).get();
		if (isActive) {
			post.setStatusId(PostStatus.ACTIVE.getValue());
		} else {
			post.setStatusId(PostStatus.INACTIVE.getValue());
		}
		return postRepository.save(post);
	}

}
