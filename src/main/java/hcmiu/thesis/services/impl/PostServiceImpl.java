package hcmiu.thesis.services.impl;

import hcmiu.thesis.commons.SystemMessage;
import hcmiu.thesis.dtos.PostDTO;
import hcmiu.thesis.models.Post;
import hcmiu.thesis.models.PostCategory;
import hcmiu.thesis.repositories.EntityManagerRepository;
import hcmiu.thesis.repositories.PostCategoryRepository;
import hcmiu.thesis.repositories.PostRepository;
import hcmiu.thesis.repositories.SummarizationDao;
import hcmiu.thesis.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class PostServiceImpl implements PostService {

	@Autowired
	private SummarizationDao summarizationDao;
	
	@Autowired
	private EntityManagerRepository repository;
	
	@Autowired
	private PostRepository postRepo;
	
	@Autowired
	private PostCategoryRepository postCategoryRepo;
	
	public PostDTO getPostById(Integer id) {
		return repository.getPostById(id);
	}
	
	@Override
	public List<PostDTO> getAllPostsByStatus(Integer status) {
        return repository.getAllPostsByStatus(status);
    }
    
	@Override
	public String getSummary(String text, int sentenceNum) {
		return summarizationDao.getSummary(text,sentenceNum);
	}
	@Override
	public String getLSA(String text, int number) {
		return summarizationDao.getLSA(text, number);
	}
	
       
	@Override
	public int getLastPage(int numberOfItem, int itemsPerPage) {
		int lastPage;

		if (numberOfItem % itemsPerPage == 0) {
			lastPage = numberOfItem / itemsPerPage;

		} else {
			lastPage = numberOfItem / itemsPerPage + 1;
		}
		return lastPage;
	}
	//method get start of pagination
	@Override
	public int getStartOfPagination(int page, int numberOfPaginationIndex) {
		int start;
		if (page == 0){
			return 1;
		}
		if (page % numberOfPaginationIndex == 0) {
			start = (page / numberOfPaginationIndex - 1) * numberOfPaginationIndex + 1;
		} else {
			start = (page / numberOfPaginationIndex) * numberOfPaginationIndex + 1;
		}
		return start;
	}

	@Override
	public List<PostDTO> getAllPostsByCategory(Integer status, String category) {
		return getAllPostsByStatus(status)
		.stream()
		.filter(a -> a.getCategoryName().equals(category))
		.collect(Collectors.toList());
	}

	@Override
	public List<PostDTO> getAllPostsByYear(Integer status, Integer year) {
		return getAllPostsByStatus(status)
		.stream()
		.filter(a -> year.equals(a.getCreateTime().getYear()))
		.collect(Collectors.toList());
	}
	
	@Override
	public List<PostDTO> getAllPostsByUsername (String username) {
		return getAllPostsByStatus(1)
		.stream()
		.filter(each -> each.getUsername().equals(username))
		.collect(Collectors.toList());
	}

	@Override
	public List<PostDTO> getAllPosts() {
		return repository.getAllPosts();
	}

	@Override
	public List<Post> getListOfPosts() {
		return postRepo.findAll();
	}

	@Override
	public String deletePostById(Integer id) {
		Boolean checkPost = postRepo.existsById(id);
		if (checkPost) {
			postRepo.deleteById(id);
			return SystemMessage.SUCCESSFUL_DELETE;
		} else {
			return SystemMessage.POST_NOT_FOUND;
		}
	}
	
	@Override
	public String changePostStatusById (Integer id, Integer status) {
		Boolean checkPost = postRepo.existsById(id);
		if (checkPost.equals(true)) {
			Post post = postRepo.findById(id).get();
			post.setStatusId(status);
			postRepo.save(post);
			return SystemMessage.SUCCESSFUL_DELETE;
		} else {
			return SystemMessage.POST_NOT_FOUND;
		}
	}

	@Override
	public String changePostStatusById(List<Integer> ids, Integer status) {
		List<Post> listPost = postRepo.findAllById(ids);
		listPost.forEach(post -> {
			post.setStatusId(status);
		});
		return SystemMessage.SUCCESSFUL_DELETE;
	}

	@Override
	public void updateAbstract() {
		List<Post> listPosts = postRepo.findAll();
		listPosts.stream().map(post -> {
			post.setAbstracts(summarizationDao.getSummary(post.getContent(), 2));
			return post;
		}).collect(Collectors.toList());
		postRepo.saveAll(listPosts);
	}

	@Override
	public Boolean createPost(Post post, PostCategory postCategory) {
		post.setStatusId(2);
		Optional<Post> isSaved = Optional.ofNullable(postRepo.save(post));
		postCategory.setPostId(postRepo.findByTitleAndUserId(post.getTitle(), post.getUserId()).getPostId());
		postCategoryRepo.save(postCategory);
		return isSaved.isPresent();
	}

	@Override
	public List<PostDTO> findPostsByText(String text) {
		return repository.findPostByText(text)
				.stream()
				.filter(post -> post.getStatusName().equals("Active"))
				.collect(Collectors.toList());
	}
}
