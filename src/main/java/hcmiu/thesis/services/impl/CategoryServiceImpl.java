package hcmiu.thesis.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hcmiu.thesis.dtos.CategoryDTO;
import hcmiu.thesis.repositories.CategoryRepository;
import hcmiu.thesis.services.CategoryService;
import hcmiu.thesis.utils.mappers.CategoryMapper;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	CategoryRepository categoryRepository;
	@Override
	public List<CategoryDTO> getAllCategories() {
		return categoryRepository.findAll()
		.stream().map(CategoryMapper::toDto)
		.collect(Collectors.toList());
	}

}
