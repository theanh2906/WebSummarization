package hcmiu.thesis.services.impl;

import hcmiu.thesis.commons.CommentTypes;
import hcmiu.thesis.commons.PostStatus;
import hcmiu.thesis.dtos.CommentDTO;
import hcmiu.thesis.dtos.ReplyDTO;
import hcmiu.thesis.models.Comment;
import hcmiu.thesis.repositories.AccountReposity;
import hcmiu.thesis.repositories.CommentRepository;
import hcmiu.thesis.repositories.EntityManagerRepository;
import hcmiu.thesis.repositories.UserRepository;
import hcmiu.thesis.services.CommentService;
import hcmiu.thesis.utils.mappers.CommentMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CommentServiceImpl implements CommentService{
	
	@Autowired
	private EntityManagerRepository entityRepo;

	@Autowired
	private CommentRepository commentRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AccountReposity accountReposity;

	@Override
	public List<CommentDTO> getAllComments() {
		return entityRepo
				.getAllComments()
				.stream()
				.filter(c -> c.getType().equals(CommentTypes.COMMENT.getValue()))
				.collect(Collectors.toList());
	}

	@Override
	public List<ReplyDTO> getAllRepliesByCommentId(Integer id) {
		return entityRepo
				.getAllComments()
				.stream()
				.filter(c -> c.getType().equals(CommentTypes.REPLY.getValue()) && c.getReplyTo().equals(id))
				.map(each ->
					new ReplyDTO(each.getCommentId(), each.getPostId(), each.getUsername(), each.getAvatarPath(), each.getContent(), each.getType(), each.getCreateTime(), each.getReplyTo())
				).collect(Collectors.toList());
	}

	@Override
	public List<CommentDTO> getCommentsDtoByPostId(Integer id) {
		List<CommentDTO> listCommentDTO = new ArrayList<>();
		List<CommentDTO> listCommentByPostId = getAllComments().stream().filter(c -> c.getPostId().equals(id)).collect(Collectors.toList());
		listCommentByPostId.forEach(each -> {
			CommentDTO commentDTO = new CommentDTO();
			BeanUtils.copyProperties(each, commentDTO);
			commentDTO.setReplies(getAllRepliesByCommentId(each.getCommentId()));
			listCommentDTO.add(commentDTO);
		});
		
		return listCommentDTO;
	}

	@Override
	public Boolean createComment(CommentDTO comment) {
		final Comment model = CommentMapper.toModel(comment);
		model.setStatus(PostStatus.ACTIVE.getValue());
		model.setUserId(userRepository.findByAccountId(accountReposity.findByUsername(comment.getUsername()).getAccountId()).getUserId());
		Optional<Comment> isSaved = Optional.ofNullable(commentRepository.save(model));
		return isSaved.isPresent();
	}
}
