package hcmiu.thesis.services.impl;

import static hcmiu.thesis.commons.EmailTypes.SUCCESSFUL_REGISTRATION;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hcmiu.thesis.dtos.RegisterInfo;
import hcmiu.thesis.models.Account;
import hcmiu.thesis.repositories.AccountReposity;
import hcmiu.thesis.repositories.UserRepository;
import hcmiu.thesis.services.AuthenticationService;
import hcmiu.thesis.services.MailService;
import hcmiu.thesis.utils.StringUtil;

@Service
public class AuthenticationServiceImpl implements AuthenticationService{

	@Autowired
	AccountReposity accountRepo;
	@Autowired
	MailService mailService;
	@Autowired
	UserRepository userRepository;
	@Override
	public boolean checkExistAccount(String username) {
		Optional<Account> account = Optional.of(accountRepo.findByUsername(username));
		return account.isPresent();
	}
	@Override
	public String register(RegisterInfo info) {
		info.setPassword(StringUtil.encode(info.getPassword()));
		Account account = new Account();
		BeanUtils.copyProperties(info, account);
		account.setIsActive(Boolean.FALSE);
		account.setRole(2);
		accountRepo.save(account);
		mailService.sendMail(info, SUCCESSFUL_REGISTRATION);
		return null;
	}
	@Override
	public Integer findUserIdByUsername(String username) {
		Integer accountId = accountRepo.findByUsername(username).getAccountId();
		return userRepository.findByAccountId(accountId).getUserId();
	}
	
	
}
