package hcmiu.thesis.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import hcmiu.thesis.commons.SystemMessage;
import hcmiu.thesis.models.Account;
import hcmiu.thesis.repositories.AccountReposity;

@Service
public class UserServiceImpl implements UserDetailsService {

	@Autowired
	AccountReposity accountReposity;
	
	@Autowired
	BCryptPasswordEncoder bcrypt;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = accountReposity.findActiveAccountByUsername(username);
		
		UserBuilder builder;
		if (account != null) {
			builder = User.withUsername(username);
			builder.password(account.getPassword());
			if (account.getRole() == 1) {
				builder.roles("ADMIN");
			} else if (account.getRole() == 2) {
				builder.roles("USER");
			}
		} else {
			throw new UsernameNotFoundException(SystemMessage.USER_NOT_FOUND);
		}
		return builder.build();
	}

}
