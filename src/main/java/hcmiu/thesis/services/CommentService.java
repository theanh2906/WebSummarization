package hcmiu.thesis.services;

import hcmiu.thesis.dtos.CommentDTO;
import hcmiu.thesis.dtos.ReplyDTO;

import java.util.List;

public interface CommentService {
	List<CommentDTO> getAllComments () ;
	
	List<ReplyDTO> getAllRepliesByCommentId (Integer id) ;

	List<CommentDTO> getCommentsDtoByPostId(Integer id);

	Boolean createComment(CommentDTO comment);
}
