package hcmiu.thesis.services;

import java.util.List;
import java.util.Map;

import hcmiu.thesis.models.Post;
import hcmiu.thesis.models.Rating;


public interface RatingService {
	
	List<Rating> getAllRatings ();
	
	List<Rating> getRatingsByPostId (Integer id);
	
	Map<String, Integer> getRatingsByPostIdFilterByStar(Integer id) ;
	
	Double calculateOverall(Integer id);
	
	List<Post> updateOverall ();

	Map<String, String> getPercentageOfRating(Map<String, Integer> map);
}
