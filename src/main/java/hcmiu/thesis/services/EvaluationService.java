package hcmiu.thesis.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import hcmiu.thesis.commons.ContentTypes;
import hcmiu.thesis.commons.HttpMethods;
import hcmiu.thesis.dtos.Summary;
import hcmiu.thesis.dtos.TestData;
import hcmiu.thesis.repositories.SummarizationDao;
import hcmiu.thesis.repositories.impl.SummarizationDaoImpl;
import hcmiu.thesis.utils.WordUtil;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.math3.util.Precision;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriUtils;

import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class EvaluationService{

    private SummarizationDaoImpl summarizationDao = new SummarizationDaoImpl();
    private static String SENTENCE_NUMBER = "5";
    private static final String API_ENDPOINT_URL = "https://textanalysis-text-summarization.p.rapidapi.com/text-summarizer-text";
    private static final String X_RAPIDAPI_KEY = "1fd7453413mshb4dfa37bad68c38p110e8cjsn071e34248972";
    private static final String X_RAPIDAPI_HOST = "textanalysis-text-summarization.p.rapidapi.com";
//    private static final String SUMMARIZATION_ENDPOINT = "http://localhost/summarization/text";

    public static EvaluationService getInstance () {
        return new EvaluationService();
    }

    public static List<TestData> readCsvAsList(String path) throws IOException {
        List<TestData> listData = new ArrayList<>();
        Reader reader = Files.newBufferedReader(Paths.get(path), StandardCharsets.ISO_8859_1);
        CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withIgnoreEmptyLines().withIgnoreEmptyLines().withTrim());
        for (CSVRecord record : csvParser)  {
            listData.add(new TestData(record.get(1), SENTENCE_NUMBER));
        }
        return listData;
    };

    public static List<String> getSummaryByText(String text, Integer numberOfSentences) throws Exception {
        text = UriUtils.encodePath(text.replaceAll("\n", " "), StandardCharsets.UTF_8);
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(API_ENDPOINT_URL))
                .header("content-type", ContentTypes.APPLICATION_FORM_URLENCODED.value())
                .header("x-rapidapi-key", X_RAPIDAPI_KEY)
                .header("x-rapidapi-host", X_RAPIDAPI_HOST)
                .method(HttpMethods.POST, HttpRequest.BodyPublishers.ofString(String.format("text=%s&sentnum=%s", text, numberOfSentences)))
                .build();
        ObjectMapper objectMapper = new ObjectMapper();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        return objectMapper.readValue(response.body(), Summary.class).getSentences();
    }

    public static String getSummary(String text, Integer numSent) throws Exception {
        return WordUtil.concatList(getSummaryByText(text, numSent));
    }

    public static double calculateFValue (String text1, String text2) throws IOException {
        double recall = (double) WordUtil.countSameSentences(text1, text2) / 5;
        double precision = (double) WordUtil.countSameSentences(text1, text2) / 5;
        if (WordUtil.countSameSentences(text1, text2) == 0) {
            return 0;
        }
        double fValue = 2 * ((recall * precision) / (recall + precision));
        return Precision.round(fValue, 4);
    }

}
