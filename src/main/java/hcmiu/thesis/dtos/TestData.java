package hcmiu.thesis.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TestData {
    @JsonIgnore
    private String title;
    private String textInput;
    private String sentNum;

    public TestData(String textInput, String sentNum) {
        this.textInput = textInput;
        this.sentNum = sentNum;
    }
}
