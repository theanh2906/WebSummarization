package hcmiu.thesis.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import hcmiu.thesis.utils.annotations.validators.CheckEmail;
import hcmiu.thesis.utils.annotations.validators.CheckUsername;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterInfo {
	
	@Size (min = 4, max = 20, message = "Username should be between 4 and 20 and not empty")
	@CheckUsername
	private String username;
	
	@Email (message = "Invalid email format. Please input again")
	@Size (min = 4, max = 20, message = "Email should be between 4 and 20 and not empty")
	@CheckEmail
	private String email;
	
	@Size (min = 4, max = 20, message = "Password should be between 4 and 20 and not empty")
	private String password;
}
