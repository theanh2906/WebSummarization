package hcmiu.thesis.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CategoryCountDTO {
	
	private static CategoryCountDTO INSTANCE;
	private Integer categoryId;
	private String categoryName;
	private Integer numberOfPosts;
	public CategoryCountDTO(String categoryName, Integer numberOfPosts) {
		super();
		this.categoryName = categoryName;
		this.numberOfPosts = numberOfPosts;
	}
	public CategoryCountDTO(Integer categoryId, String categoryName) {
		super();
		this.categoryId = categoryId;
		this.categoryName = categoryName;
	}
	
	public static CategoryCountDTO getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new CategoryCountDTO();
		}
		return INSTANCE;
	}
}
