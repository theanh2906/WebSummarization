

// var myEditor;
// ClassicEditor
// 	.create( document.querySelector( '#content' ), {
//
// 	} )
// 	.then( editor => {
// 		window.editor = editor;
// 		myEditor = editor;
// 	} )
// 	.catch( err => {
// 		console.error( err.stack );
// 	} );


$("#generate-text").on("click", function(e){
	e.preventDefault();
	
	$.get("/summarization/test/text", function(data) {
		//$("textarea#content").val(data);
		myEditor.setData(data);
	})
});

$("#reset").on("click", function(e){
	myEditor.setData("");
/*	$("#title").val("");
	$("#abstracts").val("");*/
})

$("#generate-abstract").on("click", function(e){
	e.preventDefault();
	if (myEditor.getData()!=="") {
	var data = myEditor.getData().replace( /(<([^>]+)>)/ig, ' ').trim();
	var text = { content : data };
	$.post("/summarization/test/abstracts", text, function(res1) {
		$("#abstracts").val( res1 );
	})
	} else {
		alert("null");
	}
})

$("#recommendCategory").on("click", function(event){
	event.preventDefault();
	if (myEditor.getData()!=="") {
	var data = myEditor.getData().replace( /(<([^>]+)>)/ig, ' ').trim();
	$.post("/summarization/test/doc-cat", data, function(res) {
		alert( res );
	})
	} else {
		alert("null");
	}
})

$('#content').summernote({
	tabsize: 2,
	height: 120,
	toolbar: [
		['style', ['style']],
		['font', ['bold', 'underline', 'clear']],
		['color', ['color']],
		['para', ['ul', 'ol', 'paragraph']],
		['table', ['table']],
		['insert', ['link', 'picture', 'video']],
		['view', ['fullscreen', 'codeview', 'help']]
	]
})

// function show() {
// 	$("#show").html($('.note-editable').html())
// }
