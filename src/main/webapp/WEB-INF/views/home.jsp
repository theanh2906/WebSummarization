<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="time" %>
<%@ page import="hcmiu.thesis.commons.WebConstant" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>${WebConstant.WEBSITE_NAME}</title>
    <%@include file="commons/css_import.jsp" %>
    <base href="/summarization/">
</head>

<body>
<div class="container">

    <!-- Navbar -->
    <%@ include file="components/header.jsp" %>
    <!-- End Navbar -->
    <div>
        <%@ include file="components/jumbotron.jsp" %>
    </div>

    <%@include file="components/carousel.jsp" %>
</div>

<main role="main" class="container">
    <div class="row">
        <div class="col-md-8 blog-main">
            <c:if test="${not empty message}">
                <h1>${message}</h1>
                <hr class="mb-5">
            </c:if>
            <c:forEach items="${listPost}" var="post">
                <div class="blog-post">
                    <a href="home?category=${post.categoryName}">
                        <span id="cat"
                              class="badge badge-pill p-2"
                              style="background-color: ${post.code};color:white;font-size:14px;font-weight:500">${post.categoryName}
                        </span>
                    </a>
                    <h2 class="blog-post-title">${post.title}</h2>
                    <time:format value="${post.createTime}" pattern="yyyy-MM-dd HH:mm:ss" var="date" style="F"/>${date}
                    by <a href="#">${post.author}</a></p>

                    <p>${post.abstracts}</p>
                    <a href="post/${post.postId}">Click here to view details...</a>
                    <hr>
                        <%-- ${article.content} --%>
                </div>
                <!-- /.blog-post -->

            </c:forEach>

            <nav class="blog-pagination">
                <a class="btn btn-outline-primary" href="#">Older</a>
                <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
            </nav>

        </div><!-- /.blog-main -->

        <%@include file="components/SideBar.jsp" %>

    </div><!-- /.row -->


</main><!-- /.container -->
<!-- /.container -->
<%@include file="components/footer.jsp" %>

<!-- Bootstrap core JavaScript -->
<%--<%@ include file="commons/js_import.jsp" %>--%>

</body>
</html>
