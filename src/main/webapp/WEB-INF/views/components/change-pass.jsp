<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Title</title>
<%@ include file="../commons/css_import.jsp"%>
<link rel="stylesheet" type="text/css" href="assets/css/login-register.css"/>
</head>
<body>
	<div id="logreg-forms">
		<form>
			<h1 class="h3 mb-3 font-weight-normal" style="text-align: center">
				Account confirmed!</h1>
			<p>Please click the button below to login</p>
			<button class="btn btn-success btn-block" type="button" id="btn-login">
				<i class="fas fa-sign-in-alt"></i> Login
			</button>
		</form>
	</div>
	<%@ include file="../commons/js_import.jsp"%>
	<script type="text/javascript">
    $('#btn-login').on("click", function () {
		window.location.href="http://web-demo.top/summarization/login"
	}); // display:block or none
	</script>
</body>
</html>
