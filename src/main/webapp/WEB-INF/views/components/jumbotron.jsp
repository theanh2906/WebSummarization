<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Title</title>
		<%@include file="../commons/css_import.jsp"%>
	</head>
	<body>
    	<div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
			<div class="col-md-6 px-0">
				<h1 class="display-4 font-italic">Title of a longer featured
					blog post</h1>
				<p class="lead my-3">Multiple lines of text that form the lede,
					informing new readers quickly and efficiently about what's most
					interesting in this post's contents.</p>
				<p class="lead mb-0">
					<a href="#" class="text-white font-weight-bold">Continue
						reading...</a>
				</p>
			</div>
		</div>
	</body>
</html>
