<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="time" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Insert title here</title>
    <%@include file="../commons/css_import.jsp" %>
    <link rel="stylesheet" href="/summarization/assets/plugins/slick/slick-theme.css">
</head>
<body>
<h3>Latest articles</h3>
<section class="regular slider container mt-3">
    <c:forEach items="${latestNews}" var="eachNews">
        <div style="padding: 5px">
            <span class="badge badge-pill p-2" style="background-color: ${eachNews.code};color:white;font-size:14px;font-weight:500">${eachNews.categoryName}</span>
            <a href="/summarization/post/${eachNews.postId}">${eachNews.title}</a>
        </div>
    </c:forEach>
</section>
<%@include file="../commons/js_import.jsp" %>
<script type="text/javascript">
    $(".regular").slick({
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000
    });
</script>
</body>
</html>