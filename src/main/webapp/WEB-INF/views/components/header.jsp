<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="security"
           uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" charset="UTF-8"
          content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/styles.css">
    <base href="/summarization/">
</head>
<body>
<header class="blog-header py-3">
    <div
            class="row flex-nowrap justify-content-between align-items-center">
        <div class="col-4">
            <a class="blog-header-logo text-dark" href="#">The online
                article</a>
        </div>
    </div>
</header>
<c:if test="${not empty status }">
    <div class="alert alert-${status}">${responseMessage}</div>
</c:if>
<nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
    <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <security:authorize access="!hasRole('ADMIN')">
                <li class="nav-item active">
                    <a class="nav-link" href="home">
                        <em class="fas fa-home" style="font-size: 25px;"></em>
                        <span class="sr-only">(current)</span>
                </a></li>
                <li class="nav-item dropdown"><a
                        class="nav-link dropdown-toggle" href="#" id="category"
                        role="button" data-toggle="dropdown" data-hover="dropdown"
                        aria-haspopup="true" aria-expanded="false">Categories</a>
                    <div class="dropdown-menu" aria-labelledby="category">
                        <c:forEach items="${allCategories}" var="each">
                            <a class="dropdown-item"
                               href="home?category=${each.categoryName}">${each.categoryName}</a>
                        </c:forEach>
                    </div>
                </li>
                <li class="nav-item dropdown"><a
                        class="nav-link" href="create-post"> Create new post </a>
                </li>
                <li class="nav-item dropdown"><a
                        class="nav-link dropdown-toggle" href="#"
                        id="dropdownTools" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"> Tools </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownTools">
                        <a class="dropdown-item" href="#">Document to text</a>
                    </div>
                </li>
                <li class="nav-item ml-5"></li>
            </security:authorize>

            <security:authorize access="hasRole('ADMIN')">
                <li class="nav-item active mr-5">
                    <a class="nav-link" href="admin/home">
                        <em class="fas fa-home" style="font-size: 25px;"></em>
                        <span class="sr-only">(current)</span>
                    </a></li>
                <a href="admin/account-management"><button class="btn btn-outline-primary mr-2">Manage Accounts</button></a>
                <a href="admin/post-management"><button class="btn btn-outline-primary mr-2">Manage Posts</button></a>
            </security:authorize>
        </ul>
        <div class="dropdown mr-4">
            <security:authorize access="!isAuthenticated()">
                <a href="login">
                    <button
                            class="btn btn-outline-primary mr-2">Login / Register
                    </button>
                </a>
            </security:authorize>
            <security:authorize access="isAuthenticated()">
                Hello, <security:authentication property="principal.username"/>
                <a class="dropdown-toggle" href="#" id="accountControl"
                   role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false"> <img
                        src="assets/img/avatar/avatar1.png"
                        class="avatar" alt="none">
                </a>
                <div class="dropdown-menu" aria-labelledby="accountControl">
                    <a class="dropdown-item" href="profile">Profile</a>
                    <a class="dropdown-item" href="#">Another action</a> <a
                        class="dropdown-item" href="logout">Logout</a>
                </div>
            </security:authorize>
        </div>
        <form class="form-inline my-2 my-lg-0" method="get" action="home" enctype="text/plain">
            <input class="form-control mr-sm-2" type="search" name="text"
                   placeholder="Search" aria-label="Search">
        </form>
    </div>
</nav>
</body>
</html>
