<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" charset="UTF-8" content="width=device-width, initial-scale=1.0">
<%@include file="../commons/css_import.jsp"%>
</head>
<body>
	<!-- Footer -->
	<footer class="blog-footer font-small bg-light pt-4">

<%--		<!-- Footer Links -->--%>
<%--		<div class="text-center text-md-left container">--%>

<%--			<!-- Grid row -->--%>
<%--			<div class="row">--%>

<%--				<!-- Grid column -->--%>
<%--				<div class="col-md-6 mt-md-0 mt-3">--%>

<%--					<!-- Content -->--%>
<%--					<h5 class="text-uppercase">Footer Content</h5>--%>
<%--					<p>Here you can use rows and columns to organize your footer--%>
<%--						content.</p>--%>

<%--				</div>--%>
<%--				<!-- Grid column -->--%>

<%--				<hr class="clearfix w-100 d-md-none pb-3">--%>

<%--				<!-- Grid column -->--%>
<%--				<div class="col-md-3 mb-md-0 mb-3">--%>

<%--					<!-- Links -->--%>
<%--					<h5 class="text-uppercase">Links</h5>--%>

<%--					<ul class="list-unstyled">--%>
<%--						<li><a href="#!">Link 1</a></li>--%>
<%--						<li><a href="#!">Link 2</a></li>--%>
<%--						<li><a href="#!">Link 3</a></li>--%>
<%--						<li><a href="#!">Link 4</a></li>--%>
<%--					</ul>--%>

<%--				</div>--%>
<%--				<!-- Grid column -->--%>

<%--				<!-- Grid column -->--%>
<%--				<div class="col-md-3 mb-md-0 mb-3">--%>

<%--					<!-- Links -->--%>
<%--					<h5 class="text-uppercase">Links</h5>--%>

<%--					<ul class="list-unstyled">--%>
<%--						<li><a href="#!">Link 1</a></li>--%>
<%--						<li><a href="#!">Link 2</a></li>--%>
<%--						<li><a href="#!">Link 3</a></li>--%>
<%--						<li><a href="#!">Link 4</a></li>--%>
<%--					</ul>--%>

<%--				</div>--%>
<%--				<!-- Grid column -->--%>

<%--			</div>--%>
<%--			<!-- Grid row -->--%>

<%--		</div>--%>
<%--		<!-- Footer Links -->--%>

		<!-- Copyright -->
		<div class="footer-copyright text-center py-3">
			© 2020 Copyright: <a href="https://mdbootstrap.com/">
				theanh2906@gmail.com</a>
		</div>
		<!-- Copyright -->

	</footer>
	<!-- Footer -->
</body>
</html>