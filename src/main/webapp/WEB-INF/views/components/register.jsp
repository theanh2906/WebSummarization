<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Title</title>
<%@ include file="../commons/css_import.jsp"%>
<link rel="stylesheet" type="text/css" href="assets/css/login-register.css" />
</head>
<body>
	<div id="logreg-forms">
		<form:form action="/summarization/register" class="form-signup"
			modelAttribute="input" method="post">
			<div class="social-login">
				<button class="btn facebook-btn social-btn" type="button">
					<span><em class="fab fa-facebook-f"></em> Sign up with
						Facebook</span>
				</button>
			</div>
			<div class="social-login">
				<button class="btn google-btn social-btn" type="button">
					<span><em class="fab fa-google-plus-g"></em> Sign up with
						Google+</span>
				</button>
			</div>

			<p style="text-align: center">OR</p>
			<form:errors path="username" cssClass="warn"/>
			<form:input type="text" id="username" class="form-control"
				placeholder="Username" autofocus="" path="username" />
			<form:errors path="email" cssClass="warn" />
			<form:input type="text" id="email" class="form-control"
				placeholder="Email address" autofocus="" path="email"/>
			<form:errors path="password" cssClass="warn"/>
			<form:input type="password" id="password" class="form-control"
				placeholder="Password" autofocus="" path="password"/>
			<form:button class="btn btn-primary btn-block" type="submit">
				<em class="fas fa-user-plus"></em> Sign Up
			</form:button>
			<a href="/summarization/login" id="cancel_signup"><em class="fas fa-angle-left"></em>
				Login</a>
		</form:form>
	</div>
	<script type="text/javascript">
		
	</script>
</body>
</html>
