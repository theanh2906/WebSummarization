<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Untitled Document</title>
	<base href="/summarization/demo/">
	<%@include file="commons/css_import.jsp" %>
</head>
<style>
	h1 {
		color: red;
	}
</style>
<body class="container-fluid">
<h1 class="text-center title">Summarization Tools</h1>

<form:form class="row" method="POST" action="submit" modelAttribute="input" acceptCharset="UTF-8">
	<div class="form-group col-6">
		<label for="textInput">Input text here</label>
		<form:textarea path="textInput" class="form-control" rows="8" />
	</div>
	<div class="form-group col-6">
		<label>Output</label>
		<textarea class="form-control" rows="8" readonly>${summary}</textarea>
	</div>
	<div class="form-group col-6">
		<label for="method">Type</label> <br>
		<form:radiobutton
				name="method"
				value="wf"
				path="method"
				checked="checked"/> Word Frequency
		<form:radiobutton
				name="method"
				value="lsa"
				path="method" /> Singular Value Decomposition<br>
		<br> <label>Number of sentences:</label><br>
		<form:input path="sentenceNum" />
	</div>
	<div class="form-group col-6">
		<label>Open file:</label> <br> <input type="file"
											  name="inputFile" id="inputFile">
	</div>
	<div class="form-group col-12 text-center">
		<textarea class="form-control" rows="8" readonly><c:out
				value="${stat}"></c:out></textarea>
		<br>
		<form:button type="submit" class="btn btn-primary mr-2">Submit</form:button>
		<form:button type="reset" class="btn btn-primary mr-2">Reset</form:button>
		<button type="button" onclick="random()" class="btn btn-primary mr-2">Random</button>
<%--		<button class="btn btn-primary mr-2">Compare</button>--%>
		<!-- <button class="btn btn-primary mr-2">Show graph</button> -->
	</div>
</form:form>
<%@include file="commons/css_import.jsp" %>


<script type="text/javascript">
	document.getElementById('inputFile').addEventListener('change', function(){
		var fr = new FileReader();
		fr.onload = function() {
			document.getElementById('textInput').innerHTML = fr.result;
		}
		fr.readAsText(this.files[0]);
	})
</script>
</body>
</html>
