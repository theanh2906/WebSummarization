<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="time" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"	rel="stylesheet">
		<title>${headerTitle}</title>
		<base href="/summarization/">
		<!-- Bootstrap core CSS -->
		<%@include file="commons/css_import.jsp"%>
	
		<!-- Custom styles for this template -->
		<style>

			.heading {
			  font-size: 25px;
			  margin-right: 25px;
			}
			
			.fa {
			  font-size: 25px;
			}
			
			.checked {
			  color: orange;
			}
			
			/* Three column layout */
			.side {
			  float: left;
			  width: 15%;
			  margin-top:10px;
			}
			
			.middle {
			  margin-top:10px;
			  float: left;
			  width: 70%;
			}
			
			/* Place text to the right */
			.right {
			  text-align: right;
			}
			
			/* Clear floats after the columns */
			.row:after {
			  content: "";
			  display: table;
			  clear: both;
			}
			
			/* The bar container */
			.bar-container {
			  width: 100%;
			  background-color: #f1f1f1;
			  text-align: center;
			  color: white;
			}
			
			/* Individual bars */
			.bar-5 {height: 18px; background-color: #4CAF50;}
			.bar-4 {height: 18px; background-color: #2196F3;}
			.bar-3 {height: 18px; background-color: #00bcd4;}
			.bar-2 {height: 18px; background-color: #ff9800;}
			.bar-1 {height: 18px; background-color: #f44336;}
			
			/* Responsive layout - make the columns stack on top of each other instead of next to each other */
			@media (max-width: 400px) {
			  .side, .middle {
			    width: 100%;
			  }
			  .right {
			    display: none;
			  }
			}
		</style>
	
	</head>

	<body>
		<main role="main" class="container">
		<%@ include file="components/header.jsp" %>
		<%@ include file="components/modal.jsp" %>
			<div class="row mt-5">
				<div class="col-md-8 blog-main">
					<div class="blog-post">
						<h2 class="blog-post-title">${post.title}</h2>
						<p class="blog-post-meta">
							<time:format value="${post.createTime}" pattern="yyyy-MM-dd HH:mm:ss" var="date" style="F"/>${date} by <a href="#">${post.author}</a>
						</p>
	
						<p>${post.abstracts}</p>
						<hr>
						${post.content}
					</div>
					<!-- /.blog-post -->
					<p>Comment:</p>
					<%-- <span><%@ include file="components/Rating.jsp"%></span> --%>

					<security:authorize access="!isAuthenticated()">
						<div class="text-center" style="margin-bottom: 70px">
							<h3>Please login to comment</h3>
							<div class="btn-group">
								<a role="button" href="authenticated/post/${post.postId}" class="btn btn-primary">Login</a>
							</div>
						</div>
					</security:authorize>
					<!-- Begin comment text box -->
					<security:authorize access="isAuthenticated()">
<%--						<div class="form-group" style="margin-bottom: 70px;">--%>
<%--						<textarea id="content" class="form-control mb-2"--%>
<%--								  placeholder="Write you comment here" rows="3"></textarea>--%>
<%--							<button type="submit" id="submit"--%>
<%--									class="btn btn-primary form-control col-3 float-right"--%>
<%--									data-toggle="modal"--%>
<%--									data-target="#messageModal">Submit</button>--%>
<%--						</div>--%>
						<div class="form-group" style="margin-bottom: 70px;">
							<div id="content"></div>
							<button type="submit" id="submit"
									class="btn btn-primary form-control col-3 float-right"
									data-toggle="modal"
									data-target="#messageModal">Submit</button>
							<div id="show"></div>
						</div>
					</security:authorize>

					<!-- End comment text box -->
					<hr>
					<!-- Begin user review -->
					<div class="p-4 mb-10">
						<div class="row">
							<div class="side">
								<div>★★★★★</div>
							</div>
							<div class="middle">
								<div class="bar-container">
									<div class="bar-5" style="width: ${listPercentage.star_5}"></div>
								</div>
							</div>
							<div class="side right">
								<div>${listRating.star_5}</div>
							</div>
							<div class="side">
								<div>★★★★</div>
							</div>
							<div class="middle">
								<div class="bar-container">
									<div class="bar-4" style="width: ${listPercentage.star_4}"></div>
								</div>
							</div>
							<div class="side right">
								<div>${listRating.star_4}</div>
							</div>
							<div class="side">
								<div>★★★</div>
							</div>
							<div class="middle">
								<div class="bar-container">
									<div class="bar-3" style="width: ${listPercentage.star_3}"></div>
								</div>
							</div>
							<div class="side right">
								<div>${listRating.star_3}</div>
							</div>
							<div class="side">
								<div>★★</div>
							</div>
							<div class="middle">
								<div class="bar-container">
									<div class="bar-2" style="width: ${listPercentage.star_2}"></div>
								</div>
							</div>
							<div class="side right">
								<div>${listRating.star_2}</div>
							</div>
							<div class="side">
								<div>★</div>
							</div>
							<div class="middle">
								<div class="bar-container">
									<div class="bar-1" style="width: ${listPercentage.star_1}"></div>
								</div>
							</div>
							<div class="side right">
								<div>${listRating.star_1}</div>
							</div>
						</div>
					</div>
					<!-- End user review -->
					<hr>
					<!-- Begin comment show -->
					<c:if test="${empty listComments}">Be the one who comment first</c:if>
				<div class="card mt-5">
				<c:forEach items = "${listComments}" var = "comment">
					<div class="card-body">
						<div class="row">
							<div class="col-md-2">
								<img src="${comment.avatarPath}"
									class="img img-rounded img-fluid" alt="none" />
								<p class="text-secondary text-center"><time:format value="${comment.createTime}" pattern="yyyy-MM-dd HH:mm:ss" var="date" style="F"/>${date}</p>
							</div>
							<div class="col-md-10">
								<p>
									<a class="float-left" href="profile/${comment.username}"><strong>${comment.username}</strong></a> 
								</p>
								<div class="clearfix"></div>
								<p>${comment.content}</p>
								<!--                         <p>
                            <a class="float-right btn btn-outline-primary ml-2"> <i class="fa fa-reply"></i> Reply</a>
                            <a class="float-right btn text-white btn-danger"> <i class="fa fa-heart"></i> Like</a>
                        </p> -->
							</div>
						</div>
						<c:forEach items = "${comment.replies}" var = "reply">
	                 <div class="card card-inner ml-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                <p class="text-secondary text-center">15 Minutes Ago</p>
                            </div>
                            <div class="col-md-10">
                                <p><a href="profile/${reply.username}"><strong>${reply.username}</strong></a></p>
                                <p>${reply.content}</p>
<!--                                 <p>
                                    <a class="float-right btn btn-outline-primary ml-2">  <i class="fa fa-reply"></i> Reply</a>
                                    <a class="float-right btn text-white btn-danger"> <i class="fa fa-heart"></i> Like</a>
                                </p> -->
                            </div>
                        </div>
                    </div>
                </div>
                </c:forEach>
					</div>
					<!-- /.card-body -->
					<hr>			
				</c:forEach>
				</div>
				<!-- /.card -->
					<!-- End comment show -->
				</div>
				<!-- /.blog-main -->
				<aside class="col-md-4 blog-sidebar">
					<h4 class="font-italic">Related Posts</h4>
					<div class="p-3 mb-3 bg-light rounded">
						<c:forEach var="each" items="${relatedPosts}">
							<span><a href="#">${each.username}</a></span> commented on <a href="post/${each.postId}">${each.title}</a><br>
						</c:forEach>
					</div>

					<div class="p-3">
						<h4 class="font-italic">All years</h4>
						<ol class="list-unstyled mb-0">
							<c:forEach items="${listYears}" var="year">
								<li><span class="badge badge-danger">${year.numberOfPosts}</span><a href="home?year=${year.year}" class="ml-2"><c:out value="${year.year}"></c:out></a></li>
							</c:forEach>

						</ol>
					</div>

					<div class="p-3">
						<h4 class="font-italic">Categories</h4>
						<ol class="list-unstyled">
							<c:forEach items="${allCategories}" var="category">
								<li><span class="badge badge-danger">${category.numberOfPosts}</span><a href="home?category=${category.categoryName}" class="ml-2">${category.categoryName}</a></li>
							</c:forEach>

						</ol>
					</div>
				</aside>
			</div>
			<!-- /.row -->
			<div style="display: none">
				<div id="postId">${post.postId}</div>
				<div id="username">${post.username}</div>
				<div id="comment-username"><security:authentication property="principal.username"/></div>
<%--				<div id="postId">${post.postId}</div>--%>
			</div>
		</main>
		<%@ include file="commons/js_import.jsp" %>
		<script src="assets/js/common.js"></script>
		<script type="text/javascript">
			$("#submit").on("click", (e) => {
				e.preventDefault();
				let textData = $('.note-editable').html();
				let json =
					{	content 	:	textData,
						postId		:	$("#postId").text(),
						type		:	"comment",
						username	:	$("#comment-username").text()
					};
				console.log(JSON.stringify(json))
				if($('.note-editable').html() !== ""){
					$.ajax({
						type: "POST",
						url : "http://localhost/summarization/comments",
						data: JSON.stringify(json),
						contentType: "application/json",
						dataType: "text",
						success: function(res) {
							$('.note-editable').html("");
							$(".modal-title").text("Successfully!");
							$(".modal-body").text("Your comment was successfully submit!");
						},
						error: function(jqXHR, textStatus, errorThrown) {
							$(".modal-title").text("Error");
							$(".modal-body").text(errorThrown);
						}
					})
				} else {
					$(".modal-body").text("Please input text");
					$(".modal-title").text("Error");
				}
			})
		</script>
	</body>
</html>
