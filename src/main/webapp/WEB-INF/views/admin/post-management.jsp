<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="time" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>${headerTitle}</title>
    <base href="/summarization/">
    <link rel="stylesheet" href="assets/css/styles.css">
    <style type="text/css">
        em {
            margin-right: 5px;
        }
        a {
            text-decoration: none;
            color: black !important;
        }
        .ban {
            cursor: not-allowed;
        }
        .status {
            font-size: 50px;
            margin: 2px 2px 0 0;
            display: inline-block;
            vertical-align: middle;
            line-height: 10px;
        }
        .text-success {
            color: #10c469;
        }
        .text-info {
            color: #62c9e8;
        }
        .text-warning {
            color: #FFC107;
        }
        .text-danger {
            color: #ff5b5b;
        }
    </style>
    <link href="blog.css" rel="stylesheet">
    <script src="/summarization/assets/plugins/js/jquery.js"></script>
    <script src="/summarization/assets/plugins/js/bootstrap.js"></script>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <link rel="stylesheet" type="text/css"
          href="/summarization/assets/plugins/css/datatables.css" />

    <script type="text/javascript" src="/summarization/assets/plugins/js/datatables.js">
    </script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#postManagement').dataTable();
        } );
    </script>
</head>
<body class="container">
<%@ include file="../components/header.jsp" %>
<h1 class="mt-4 mb-4">Posts Management</h1>
    <table id="postManagement" class="display">
        <thead>
            <tr>
                <th width="5%">Id</th>
                <th width="10%">Username</th>
                <th width="40%">Title</th>
                <th width="20%">Created date</th>
                <th width="10%">Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <c:forEach items="${allPosts}" var="post">
            <tr>
                <td scope="row">${post.postId}</td>
                <td>${post.username}</td>
                <td><a href="post/${post.postId}" target="_blank">${post.title}</a></td>
                <td><time:format value="${post.createTime}" pattern="yyyy-MM-dd HH:mm:ss" var="date" style="F"/>${date}</td>
                <td> <span class="status ${post.statusName eq 'Active' ? 'text-success' : 'text-danger'}">
						&bull;</span>
                        ${post.statusName}
                </td>
                <td>
                    <c:if test="${post.statusName eq 'Active'}">
                        <a class="ban" title="Activate" data-toggle="tooltip"><em class="fas fa-check"></em></a>
                        <a href="admin/posts/${post.postId}/status/false" title="Disable" data-toggle="tooltip"><em class="fas fa-ban"></em></a>
                        <a href="#"><em class="fas fa-user-minus" title="Delete" data-toggle="tooltip"></em></a>
                    </c:if>
                    <c:if test="${post.statusName eq 'Inactive'}">
                        <a href="admin/posts/${post.postId}/status/true" title="Activate" data-toggle="tooltip"><em class="fas fa-check"></em></a>
                        <a class="ban" title="Disable" data-toggle="tooltip"><em class="fas fa-ban"></em></a>
                        <a href="#"><em class="fas fa-user-minus" title="Delete" data-toggle="tooltip"></em></a>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</body>
</html>
