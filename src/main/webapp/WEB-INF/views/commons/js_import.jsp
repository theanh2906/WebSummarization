<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="<c:url value="/assets/plugins/js/jquery.js" />"></script>
<script src="<c:url value="/assets/plugins/js/jquery.validate.js" />"></script>
<script	src="<c:url value="/assets/plugins/js/popper.js"/>"></script>
<script	src="<c:url value="/assets/plugins/js/bootstrap.js"/>"></script>
<script src="<c:url value="/assets/plugins/js/holder.js" />"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="<c:url value="/assets/plugins/js/ckeditor.js" />"></script>
<script src="<c:url value="/assets/plugins/slick/slick.js" />"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
