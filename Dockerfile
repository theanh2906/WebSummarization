#FROM openjdk:11
FROM maven:3.6.3-jdk-11-slim

# image layer
WORKDIR /app
ADD pom.xml /app

# Image layer: with the application
COPY . /app
RUN mvn clean install -DskipTests
ENTRYPOINT ["java","-jar","./target/summarization-0.0.1.war"]
